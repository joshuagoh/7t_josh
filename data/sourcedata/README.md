# Source data directory
- **202404180834CDT_0001**: EPI, hippocampal focus for SNT on Yuhui.
- **202404171200CDT_0001**: EPI, MT with LCT-sal-event-related on Yuhui.
- **202404040924CDT_0001**: VAPER, MT with LCT-block tries on Yuhui.
- **202404031224CDT_0001**: EPI, MT with LCT-block tries on Yuhui.
- **240918GOH_JOSHUA**: T1, EPI, VAPER dicom tries on Josh.
- **AXC_sub-10**: T1, EPI dicom from BML's age by culture project sub-10 for comparison.
- **JOSHUA_CNL**: T1 dicom from CNL for Josh acquired in in 2004 for comparison.
