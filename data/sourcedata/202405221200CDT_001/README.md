# 0001 scan on 202405221200CDT
- Testing out the SNT_win_202405211218CDT (see Experimental Protocols) with 7T
- 3D EPI-BOLD sequence with MT.
- SID: 0001
- Functionals: 0.5 iso voxel, 86 coronal slices, hippocapal coverage.
- Scan Log
  1. localizer
  2. localizer
  3. bold, snt_learning_r1, < 15 min
  4. mt
  5. bold, snt_learning_r2, ~ 15 min
  6. bold, snt_testing_r1, 
  7. bold, snt_testing_r2, 
  8. bold, snt_testing_r3, 
  9. bold, snt_testing_r4,
  10. mt
  11. bold, snt_testing_r5
  12. bold, snt_testing_r6
  13. bold, snt_testing_r7
  14. bold, snt_testing_r8, response key got stuck at BestBuy

## Comments
- Check out bug at last run.
- Some test trials skipping pre-empt and feedback screens. Check timing issues in C# code.