# 0001 scan on 202404171200CDT
- Testing out the [LCT-sal](experimental_protocol/LCT_en/event-related-lct-sal) with 7T
- Trying event-related 3D EPI-BOLD sequence, with MT.
- SID: 0001
- Task inputs: left-1, right-2, trigger-= (equal)
- Functionals: 0.8 iso voxel, 86 slices, frontal, midbrain, subcortical coverage.
- Scan Log
  1. localizer
  2. bold, admt_r1, pts: -3217
  3. mt
  4. bold, admt_r2, pts: 287
  5. mt
  6. bold, admt_r3, pts: 2520
  7. mt
  8. bold, admt_r4, pts: 3951
  9. mt
  10. bold, admt_r5, pts: 8349
  11. mt, stopped to skip rest
  12. bold, admt_r6, pts: 9017

- Psychopy files stored in [beh](data/sourcedata/202404171200CDT_0001/beh)