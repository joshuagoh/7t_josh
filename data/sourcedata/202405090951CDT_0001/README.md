# 0001 scan on 202405090951CDT
- Testing out the [LCT-sal-v2](experimental_protocol/LCT_en/event-related-lct-sal-v2) with 7T
- Trying event-related 3D EPI-BOLD sequence, with MT.
- SID: 0001
- Task inputs: left-1, right-2, trigger-= (equal)
- Functionals: 0.8 iso voxel, 86 slices, frontal, midbrain, subcortical coverage.
- Scan Log
  1. localizer
  2. bold, admt_r1, pts: 3345, epi ran for 182, shortening to 181 for next round.
  3. mt
  4. bold, admt_r2, pts: 1177, shortening from 181 to 180.
  5. mt
  6. bold, admt_r3, pts: 12778, maintaining.
  7. mt, cancelled.
  8. bold, admt_r4, pts: xxx, cut halfway.
  9. mt
- Psychopy files stored in [beh](data/sourcedata/202405090951CDT_0001/beh)
- LCT-sal-v2 protocol total accumulated scores scale not ideal. Fallback to LCT-sal (v1).