# 0003 scan on 202405281200CDT
- Testing out the [LCT-sal-v1.1](experimental_protocol/LCT_en/event-related-lct-sal-v1.1) with 7T
- Trying event-related 3D EPI-BOLD sequence, with MT.
- SID: 0003
- Task inputs: left-1, right-2, trigger-= (equal)
- Functionals: 0.8 iso voxel, 86 slices, frontal, midbrain, subcortical coverage.
- Scan Log
  1. localizer
  2. bold, admt_r1, pts:, cancelled, stim not projected in scanner room, restart.
  3. bold, admt_r1, pts: -1572, a few super high response too slow in earlier trials.
  4. bold, admt_r2, pts: 2660
  5. mt
  6. bold, admt_r3, pts: 7216
  7. bold, admt_r4, pts: 7575
  8. mt
  9. bold, admt_r5, pts: 12118
  10. mt
  11. bold, admt_r6, pts: 17555

- Psychopy files stored in [beh](data/sourcedata/202405281200CDT_0003/beh)

# Comments
- Consider using longer trials for super high trials to control for anxiety.
- Change the response box buttons to index (2) vs. middle (3), blanket for position, tape squeeze bulb.
- Instruction practice: add "Just to repeat", avoid subject looking for novel info, consistency in terms of timing.
- Ask people about strategy each run, using verbal mediation for response button mapping of decision, competiveness, handedness (run right handers only?), neuropsych battery, check for head/body movements after mistakes.
- One strategy ignore magnitude, bias towards larger number rather than accept/decline.
- Consider narrowing the range of SH trial probabilities. [done: see v1.2].
- Check run3, probability > 60% for SH trials. [checked: do not see any > 60% SH trials in xlsx. Check psychopy output].
- Final text for run, have indication of number completed.
- Say close your eyes and relax during mt.
- Apply color to feedback to distinguish from choice.