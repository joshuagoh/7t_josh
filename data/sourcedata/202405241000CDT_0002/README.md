# 0002 scan on 202405241000CDT
- Testing out the SNT_win_202405211218CDT (see Experimental Protocols) with 7T
- Just doing maze-learning. 
- Maze-retrieval on separate day later.
- 3D EPI-BOLD sequence with MT.
- SID: 0001
- Functionals: 0.5 iso voxel, 86 coronal slices, hippocapal coverage.
- Scan Log
  1. localizer
  2. localizer
  3. bold, snt_learning_r1, flexi time, participants learned very quickly.
  4. bold, snt_learning_r2, halted halfway
  5. bold, snt_learning_r3, ~ 15 min
  6. mt
  7. bold, snt_testing_r1, 
  8. bold, snt_testing_r2, 
  9. bold, snt_testing_r3,
  10. mt
  11. bold, snt_testing_r4,
  12. bold, snt_testing_r5,
  13. bold, error
  13. bold, snt_testing_r6

## Comments