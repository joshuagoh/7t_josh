# 0001 scan on 202405150930CDT
- Testing out the SNT_win_202405151902 (see Experimental Protocols) with 7T
- 3D EPI-BOLD sequence with MT.
- SID: 0001
- Functionals: 0.5 iso voxel, 86 coronal slices, hippocapal coverage.
- Scan Log
  1. localizer
  2. localizer
  3. bold, snt_learning_r1
  4. bold, snt_recall_r1, stopped, trigger issue
  5. bold, snt_recall_r1, stopped, trigger issue
  6. bold, snt_recall_r1
  7. bold, sn_recall_r2, stopped, trigger issue
  8. bold, snt_recall_r2
  9. mt

## Comments
- Distance judgment phase too short, participant had no time to consider and respond.
- Trigger settings
- Add fixations ~ 20s
- Translate
- Change shops to more representative of US - Walmart, BoA, Apple, AT&T, Best Buy, Amazon, Tesla, Honda, Macy, CostCo, Macy, Target, USPS, Fedex, Dunkin Donuts, Sephora, Home Depot, Menards, 
