# 0004 scan on 202405300900CDT
- Testing out the SNT_win_202405211218CDT (see Experimental Protocols) with 7T
- 3D EPI-BOLD sequence with MT.
- SID: 0003
- Functionals: 0.5 iso voxel, 86 coronal slices, hippocapal coverage.
- Scan Log
  1. localizer
  2. localizer
  3. bold, snt_learning_r1, >400 volumes.
  4. mt_nordic
  5. mt_nordic
  6. bold, snt_learning_r2, 14min.
  7. mp2rage
  Out of scanner
  8. localizer
  9. bold, admt_r1, pts: 587
  10. mt
  11. bold, admt_r2, pts: 1053
  12. mt
  13. bold, admt_r3, pts: 1558
  14. bold, admt_r4, pts: 4221
  15. mt
  16. bold, admt_r5, pts: 3455
  17. bold, admt_r6, pts: 6230

## Comments
- Barnes&Noble blur, replace with better graphic.
- Change LCT practice 4 s to 3 s.