# 0001 scan on 202404180834CDT
Testing hippocampal hi-res BOLD.

- BOLD 0.55 x 0.55 x 0.6 mm resolution, 54 coronal slices, 10 deg tilt
- TR 7.05s
- Spatial Navigation Task (Unity3D; DSN-age)

1. Localizer
2. BOLD, Spatial Navi r1
3. MT, error
4. MT
5. BOLD, Spatial Navi r2
6. MT