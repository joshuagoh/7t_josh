# 0001 scan on 202404031224CDT
- Testing out the [LCT-block protocol](experimental_protocol/LCT_en/block) with 7T
- EPI: 0.8 iso voxel, 112 slices, frontal, midbrain, cerebellar coverage.
- SID: 0001
- Task inputs: left-1, right-2, trigger-=
- Target trying 8x LCT-block runs (repeat run 1-4 2x)
- Scan Log
  1. localizer, phantom
  2. localizer, 0001
  3. epi, admt_r1, pts: 757
  4. mt, anatomical
  5. epi, admt_r2, pts: 1228
  6. epi, admt_r3, pts: 1912
  7. mt, anatomical
  8. epi, admt_r4, pts: 2375
  9. epi, admt_r1, err: did not record csv mt was run instead
  10. epi, admt_r2, pts: 2950
  11. epi, admt_r3, pts: 3371
  - Psychopy files stored in [beh](data/sourcedata/202404031224CDT_0001/beh)
  - Notes
    - EPI, odd volumes FA=20d, even volumes FA=17d.
    - TR needs to be collected from sequence parameters, approx. 4.086 s.
    - Slice-onset timings