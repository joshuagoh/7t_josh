# 0001 scan on 202404040924CDT
- Testing out the [LCT-block protocol](experimental_protocol/LCT_en/block) with 7T
- Yesterday's scan yielded high cortical, cerebellar responses, poor midbrain, subcortical responses.
- Trying blood flow sequence today.
- SID: 0001
- Task inputs: left-1, right-2, trigger-=
- Functionals: 0.8 iso voxel, 112 slices, frontal, midbrain, cerebellar coverage.
- Scan Log
  1. localizer
  2. vaper_mt, admt_r1, pts: 709
  3. mt, admt_r2, pts: 1380
  4. vaper_mt, admt_r3, pts: 1964
  5. mt, admt_r4, pts: 2745
  6. vaper_mt, admt_r1, pts: 3283
  7. mt, admt_r2, pts: 4014
  8. vaper_mt, admt_r3, pts: 4758
  9. mt, admt_r4, pts: 5432
  10. vaper_mt, admt_r1, pts: 5862
  11. mt, admt_r2, pts: 6336
  12. vaper_mt, admt_r3, pts: 6858
  13. mt, admt_r4, pts: 7539

- Psychopy files stored in [beh](data/sourcedata/202404031224CDT_0001/beh)