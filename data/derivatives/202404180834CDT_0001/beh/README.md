## Files
- snt_XX_soa.mat - Matlab-SPM names, onsets, durations cell structures in a .mat file.
- snt_XX_soa.yaml - Conditions, onsets, durations in a YAML file.

## Conditions include:
- L - When participants are in Landmarks coordinates
- J - When participants are in Junction coordinates
- T - When participants are neither in L or J (i.e. traversing).

Onsets are in seconds from first EPI trigger and log the time participants first enter that condition coordinate.

Durations are in seconds from onset and log the time participants consecutively remain in the condition.