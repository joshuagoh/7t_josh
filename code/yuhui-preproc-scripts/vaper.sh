# # !/bin/bash

top_dir=/media/yuhui/LayMovie/Josh # replace with your own data directory
cd $top_dir

for patDir in 2404*; do
{
	cd ${top_dir}/${patDir}
	for runDir in mt.sft; do #   mt.sft 
	{	
		if [ -d ${top_dir}/${patDir}/${runDir} ]; then
			cd ${top_dir}/${patDir}/${runDir}

			run_dsets=($(ls -f bold*.nii.gz))
			run_num=${#run_dsets[@]}


			3drefit -space ORIG bold*.nii.gz dant*.nii.gz


			for subj in bold dant; do # 
			{	
				3dTcat -prefix all_runs.${subj}.nii.gz ${subj}*nii.gz -overwrite
				3dTstat -overwrite -mean -prefix mean.${subj}.nii.gz all_runs.$subj.nii.gz # note TRs that were not censored
				rm all_runs.$subj.nii.gz
			}&
			done
			wait

			3dcalc -a mean.bold.nii.gz -b mean.dant.nii.gz \
				-expr "(a-b)/(b+100)" -prefix mean.sub_d_dant.beta100.nii.gz -overwrite

			3dcalc -a mean.sub_d_dant.beta100.nii.gz -b ../brain_mask_comb.nii.gz -expr "a*step(b)" \
				-prefix mean.sub_d_dant.beta100.masked.nii.gz -overwrite

			DenoiseImage -d 3 -n Gaussian -i mean.sub_d_dant.beta100.masked.nii.gz -o mean.sub_d_dant.beta100.masked.denoised.nii.gz

			DenoiseImage -d 3 -n Gaussian -i mean.sub_d_dant.beta100.nii.gz -o mean.sub_d_dant.beta100.denoised.nii.gz


		fi
	}&
	done
	wait

}&
done
wait
