# !/bin/bash

# set data directories
top_dir=/media/yuhui/LayMovie/Josh
cd $top_dir


for patDir in 240404*; do
{
	cd ${top_dir}/${patDir}
	for runDir in vapermt.sft; do # 
	{	
		if [ -d ${top_dir}/${patDir}/${runDir} ]; then
			cd ${top_dir}/${patDir}/${runDir}

			run_dsets=($(ls -f rbold*.nii.gz))
			run_num=${#run_dsets[@]}

			trdouble=`3dinfo -tr rbold1.nii.gz`
			tr=`bc -l <<< "${trdouble}/2"`

			# # # # # # note TRs that were not censored
			# # # # # ktrs=`1d_tool.py -infile censor_combined.1D             \
			# # # # #                  -show_trs_uncensored encoded`



			for run in `seq 1 ${run_num}`; do
			{
				3dcalc -a rbold${run}.nii.gz -b rdant${run}.nii.gz \
					-expr "(a+b)/2" -prefix rbodant${run}.nii.gz -overwrite

				3dBlurInMask -input rbold${run}.nii.gz -FWHM 1.5 \
					-mask brain_mask.nii.gz -prefix blur1.rbold${run}.nii.gz -overwrite

				3dBlurInMask -input rbodant${run}.nii.gz -FWHM 1.5 \
					-mask brain_mask.nii.gz -prefix blur1.rbodant${run}.nii.gz -overwrite

				if [ -f mdant_d_mt${run}.nii.gz ]; then
					3dBlurInMask -input mdant_d_mt${run}.nii.gz -FWHM 1.5 \
						-mask brain_mask.nii.gz -prefix blur1.mdant_d_mt${run}.nii.gz -overwrite
				fi

				if [ -f sub_d_bold${run}.nii.gz ]; then
					3dBlurInMask -input sub_d_bold${run}.nii.gz -FWHM 1.5 \
						-mask brain_mask.nii.gz -prefix blur1.sub_d_bold${run}.nii.gz -overwrite
				fi

				if [ -f bold_mdant${run}.nii.gz ]; then
					3dBlurInMask -input bold_mdant${run}.nii.gz -FWHM 1.5 \
						-mask brain_mask.nii.gz -prefix blur1.bold_mdant${run}.nii.gz -overwrite
				fi

			}&
			done
			wait

			if [[ "$runDir" == *"vapermt"* ]]; then
				subj_list='rbold mdant_d_mt' #  blur1.rbold blur1.mdant_d_mt
			else
				subj_list='rbold sub_d_bold blur1.rbold blur1.sub_d_bold'
			fi

			subj_list='rbold rbodant blur1.rbold blur1.rbodant sub_d_bold bold_mdant blur1.sub_d_bold blur1.bold_mdant'

			subj_list='rbold rbodant blur1.rbold blur1.rbodant mdant_d_mt blur1.mdant_d_mt '


			for subj in ${subj_list}; do 
			{		

				if [ ! -f stats.${subj}.nii.gz ]; then
					# ================================= normalize ==================================
					# scale each voxel time series to have a mean of 100
					# (be sure no negatives creep in)
					for run in `seq 1 ${run_num}`; do
					{    
					    if [ -f ${subj}${run}.nii.gz ]; then

						    3dTstat -overwrite -prefix rm.mean.${subj}${run}.nii.gz ${subj}${run}.nii.gz
						    3dcalc -a ${subj}${run}.nii.gz -b rm.mean.${subj}${run}.nii.gz \
						    	   -c brain_mask.nii.gz -expr 'step(c)*(a/b*100)'         \
						           -prefix norm.${subj}${run}.nii.gz -overwrite

						    rm rm.mean.${subj}${run}.nii.gz
						fi
					}&
					done
					wait


					# ------------------------------
					# run the regression analysis in afni  
						# -censor censor_combined.1D   
					echo "++++++++++++++ input `ls norm.${subj}*nii.gz` for glm .........."

					# if [[ "$runDir" == *"origD"* ]]; then

					# 	# 3dDeconvolve -input norm.${subj}*nii.gz                   \
					# 	# 	-mask brain_mask.nii.gz                          \
					# 	#     -polort A -float                              \
					# 	#     -num_stimts 1                                                       \
					# 	#     -stim_times 1 ../Stim/checker_VaperOrig_dis2TR.txt 'BLOCK(36.012,1)'                    \
					# 	#     -stim_label 1 checker                                              \
					# 	#     -jobs 4                                                             \
					# 	#     -tout -nofullf_atall -x1D X.xmat.1D -xjpeg X.jpg                             \
					# 	#     -bucket stats.${subj}.nii.gz 												\
					# 	#     -overwrite

					# 	3dDeconvolve -input ${subj}*nii.gz                   \
					# 		-mask brain_mask.nii.gz                          \
					# 	    -polort A -float                              \
					# 	    -num_stimts 1                                                       \
					# 	    -stim_times 1 ../Stim/checker_VaperOrig_dis2TR.txt 'BLOCK(36.012,1)'                    \
					# 	    -stim_label 1 checker                                              \
					# 	    -jobs 4                                                             \
					# 	    -tout -nofullf_atall -x1D X.xmat.1D -xjpeg X.jpg                             \
					# 	    -bucket stats.${subj}.as.nii.gz 												\
					# 	    -overwrite

					# # elif [[ "$runDir" == *"vapermt"* ]]; then
					# else

						3dDeconvolve -input norm.${subj}*nii.gz                   \
							-mask brain_mask.nii.gz                          \
						    -polort A -float                              \
						    -num_stimts 1                                                       \
						    -stim_times 1 ../Stim/vapermt_dis2TR.txt 'BLOCK(43,1)'                    \
						    -stim_label 1 josh                                              \
						    -jobs 4                                                             \
						    -tout -nofullf_atall -x1D X.xmat.1D -xjpeg X.jpg                             \
						    -bucket stats.${subj}.nii.gz 												\
						    -overwrite

					# 	# # # # # ------------------------------
					# 	# # # # # run the regression analysis in afni 
					# 	# # # # 	# -censor censor_combined.1D                    \ 
					# 	# # # # 	# BLOCK(32.4096,1) whole brain
					# 	# # # # echo "++++++++++++++ input `ls ${subj}*nii.gz` for glm .........."
					# 	# # # # # 3dDeconvolve -input ${subj}*nii.gz                   \
					# 	# # # # # 	-mask brain_mask.nii.gz                          \
					# 	# # # # #     -polort A -float                              \
					# 	# # # # #     -num_stimts 1                                                       \
					# 	# # # # #     -stim_times 1 ../Stim/checker_VaperMT_dis2TR.txt 'BLOCK(34.2136,1)'                   \
					# 	# # # # #     -stim_label 1 checker                                              \
					# 	# # # # #     -jobs 4                                                             \
					# 	# # # # #     -tout -nofullf_atall -x1D X.xmat.1D -xjpeg X.jpg                             \
					# 	# # # # #     -bucket stats.${subj}.as.nii.gz 												\
					# 	# # # # #     -overwrite

					# 	# # BLOCK(34.2136,1) start with Xinyu
					# 	# 3dDeconvolve -input ${subj}*nii.gz                   \
					# 	# 	-mask brain_mask.nii.gz                          \
					# 	#     -polort A -float                              \
					# 	#     -num_stimts 1                                                       \
					# 	#     -stim_times 1 ../Stim/checker_VaperMT_dis2TR.txt 'BLOCK(34.2136,1)'  \
					# 	#     -stim_label 1 checker                                              \
					# 	#     -jobs 4                                                             \
					# 	#     -tout -nofullf_atall -x1D X.xmat.1D -xjpeg X.jpg                             \
					# 	#     -bucket stats.${subj}.as.nii.gz 												\
					# 	#     -overwrite

					# fi

					# 3dcalc -a stats.${subj}.as.nii.gz'[1]' -expr "a" -prefix stats.${subj}.as.tval.nii.gz -overwrite

					# # 3dcalc -a stats.${subj}.nii.gz'[1]' -expr "a" -prefix stats.${subj}.tval.nii.gz -overwrite


					# if [[ "$subj" == *"mt"* ]]; then
					# 	3dcalc -a stats.${subj}.as.nii.gz'[1]' -expr "-a" -prefix stats.${subj}.as.tval_neg.nii.gz -overwrite

					# 	# 3dcalc -a stats.${subj}.nii.gz'[1]' -expr "-a" -prefix stats.${subj}.tval_neg.nii.gz -overwrite
					# fi
					
					rm fitts.${subj}*
					# rm norm.${subj}*
				fi
			}&
			done
			wait

			rm errts* # blur*nii.gz

			# 3dcalc -a stats.mdant_d_mt.nii.gz -b stats.mdant_d_mt.nii.gz'[0]' \
			# 	-expr "a*step(10-b)*step(b+10)" -prefix stats.mdant_d_mt_despike.nii.gz \
			# 	-overwrite

			# 3dcalc -a stats.rbold.nii.gz -b stats.rbold.nii.gz'[0]' \
			# 	-expr "a*step(25-b)*step(b+25)" -prefix stats.rbold_despike.nii.gz \
			# 	-overwrite
		fi
	}&
	done
	wait

}&
done
wait
