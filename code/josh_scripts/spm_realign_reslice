#!/bin/bash
# Setup (with option to run) SPM12 realign and reslice EPI module using
# shell script invoking matlab.
#
# Usage: spm_realign_reslice inputdat
#
# inputdat - Singular EPI .nii fullpath or text filename with fullpaths
#            per row per .nii file.
#
# 20220518 Created by Josh Goh.
# 20240831 Modified by Josh Goh for 7t_josh project.

# Assign parameters
inputdat=${1}

# Call matlab with input script
unset DISPLAY
matlab -nosplash -nodesktop > matlab.out << EOF
  settings;
  [p n e] = fileparts('${inputdat}');
  if strcmp(e,'.nii'),
	 nses = 1;
	 S = {'${inputdat}'};
  else
	 S = importdata('${inputdat}');
	 nses = size(S,1);
  end
  for r = 1:nses,
	 ni = niftiinfo(S{r});
	 nvols = ni.ImageSize(4);
	 for t = 1:nvols,
		temp(t).vol = [deblank(S{r}) ',' num2str(t)];
	 end;
	 matlabbatch{1,1}.spm.spatial.realign.estwrite.data{1,r} = cellstr(strvcat(temp.vol));
  end;
  matlabbatch{1,1}.spm.spatial.realign.estwrite.eoptions.quality = 1;
  matlabbatch{1,1}.spm.spatial.realign.estwrite.eoptions.sep = 1;
  matlabbatch{1,1}.spm.spatial.realign.estwrite.eoptions.fwhm = 1;
  matlabbatch{1,1}.spm.spatial.realign.estwrite.eoptions.rtm = 1;
  matlabbatch{1,1}.spm.spatial.realign.estwrite.eoptions.interp = 5;
  matlabbatch{1,1}.spm.spatial.realign.estwrite.eoptions.wrap = [0 0 0];
  matlabbatch{1,1}.spm.spatial.realign.estwrite.eoptions.weight = {''};
  matlabbatch{1,1}.spm.spatial.realign.estwrite.roptions.which = [2 1];
  matlabbatch{1,1}.spm.spatial.realign.estwrite.roptions.interp = 5;
  matlabbatch{1,1}.spm.spatial.realign.estwrite.roptions.wrap = [0 0 0];
  matlabbatch{1,1}.spm.spatial.realign.estwrite.roptions.mask = 1;
  matlabbatch{1,1}.spm.spatial.realign.estwrite.roptions.prefix = 'r';
  save('spm_realign_reslice_7t_josh','matlabbatch');
  spm_jobman('run',matlabbatch);
exit;
EOF
