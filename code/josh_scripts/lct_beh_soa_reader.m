% Matlab function to read LCT CSV files and export stim info.
% Usage: D = lct_beh_csv_soa_reader(csv,type)
%
% D    - Structure containing condition names, onsets, durations, modulators.
% csv  - Session csv filename
% type - 'Bin','Prob_Mag','EV'
%
% Created by Josh Goh 202409141646GMT+8.

function D = lct_beh_csv_reader(csv,type)
  T = readtable(csv);

  switch type
    case 'Bin'
      Labels = unique(T.Label(1:56));
      % Choice
      for c = 1:size(Labels,1),
        D(c).name = Labels{c};
        trialno = strmatch(Labels{c},T.Label(1:56));

        % Condition dummy contingency
        dummy_vec = strmatch('Dummy',T.cond(trialno));
        trialno(dummy_vec) = [];

        D(c).onsets = T.Choice_started(trialno) - T.LongITI1_started(1);
        D(c).durations = T.ChoiceDur(trialno);
        c = c+1;
      end

      % Choice dummy contingency
      dummy_vec = strmatch('Dummy',T.cond(1:56));
      if ~isempty(dummy_vec),
        D(c).name = 'Dummy_choice';
        D(c).onsets = T.Choice_started(dummy_vec) - T.LongITI1_started(1);
        D(c).durations = T.ChoiceDur(dummy_vec);
        c = c+1;
      end
      choice_size = size(D,2);

      % Outcome
      cond = unique(T.cond(1:56));
      for c = c:(c+size(cond,1)-1),
        % Out dummy contingency
        if strmatch('Dummy',cond{c-choice_size}),
          D(c).name = 'Dummy_out';
        else
          D(c).name = cond{c-choice_size};
        end
        D(c).onsets = T.Feedback_started(strmatch(cond{c-choice_size},T.cond(1:56))) - T.LongITI1_started(1);
        D(c).durations = T.Feedback_stopped(strmatch(cond{c-choice_size},T.cond(1:56))) - T.Feedback_started(strmatch(cond{c-choice_size},T.cond(1:56)));
        c = c+1;
      end
    case 'Prob_Mag'
      % Choice
      c = 1;
      D(c).name = 'Choice';
      trialno = [1:56];

      % Trial dummy contingency
      dummy_vec = strmatch('Dummy',T.cond(trialno));
      trialno(dummy_vec) = [];

      D(c).onsets = T.Choice_started(trialno) - T.LongITI1_started(1);
      D(c).durations = T.ChoiceDur(trialno);

      % Parameter modulation contingency
      %% Modulation by Probability
      D(c).pmod(1).name = 'Prob';
      D(c).pmod(1).param = T.Win(trialno)/100;
      D(c).pmod(1).poly = 1;

      D(c).pmod(2).name = 'Prob2';
      D(c).pmod(2).param = (T.Win(trialno)/100).^2;
      D(c).pmod(2).poly = 1;

      %% Modulation by Magnitude
      D(c).pmod(3).name = 'Mag';
      D(c).pmod(3).param = T.Value(trialno);
      D(c).pmod(3).poly = 1;

      %% Modulation by Probability:Magnitude
      D(c).pmod(4).name = 'Prob:Mag';
      D(c).pmod(4).param = T.Win(trialno)/100.*T.Value(trialno);
      D(c).pmod(4).poly = 1;

      D(c).pmod(5).name = 'Prob2:Mag';
      D(c).pmod(5).param = (T.Win(trialno)/100).^2.*T.Value(trialno);
      D(c).pmod(5).poly = 1;

      c = c+1;

      % Choice dummy contingency
      dummy_vec = strmatch('Dummy',T.cond(1:56));
      if ~isempty(dummy_vec),
        D(c).name = 'Dummy_choice';
        D(c).onsets = T.Choice_started(dummy_vec) - T.LongITI1_started(1);
        D(c).durations = T.ChoiceDur(dummy_vec);
        c = c+1;
      end
      choice_size = size(D,2);

      % Outcome
      cond = unique(T.cond(1:56));
      for c = c:(c+size(cond,1)-1),
        % Out dummy contingency
        if strmatch('Dummy',cond{c-choice_size}),
          D(c).name = 'Dummy_out';
        else
          D(c).name = cond{c-choice_size};
        end
        D(c).onsets = T.Feedback_started(strmatch(cond{c-choice_size},T.cond(1:56))) - T.LongITI1_started(1);
        D(c).durations = T.Feedback_stopped(strmatch(cond{c-choice_size},T.cond(1:56))) - T.Feedback_started(strmatch(cond{c-choice_size},T.cond(1:56)));
        c = c+1;
      end
    case 'EV'
      % Choice
      c = 1;
      D(c).name = 'Choice';
      trialno = [1:56];

      % Trial dummy contingency
      dummy_vec = strmatch('Dummy',T.cond(trialno));
      trialno(dummy_vec) = [];

      D(c).onsets = T.Choice_started(trialno) - T.LongITI1_started(1);
      D(c).durations = T.ChoiceDur(trialno);

      % Parameter modulation contingency
      %% Modulation by Expected Value
      D(c).pmod(1).name = 'EV';
      EVvec = (T.Win(trialno)/100.*T.Value(trialno))+((1-T.Win(trialno)/100).*-T.Value(trialno));
      D(c).pmod(1).param = EVvec;
      D(c).pmod(1).poly = 1;

      %% Modulation by Variance
      D(c).pmod(2).name = 'Var';
      Varvec = (T.Win(trialno)/100.*(T.Value(trialno)-EVvec).^2)+((1-T.Win(trialno)/100).*(-T.Value(trialno)-EVvec).^2);
      D(c).pmod(2).param = Varvec;
      D(c).pmod(2).poly = 1;

      %% Modulation by Expected Value:Variance
      D(c).pmod(3).name = 'EV:Variance';
      D(c).pmod(3).param = EVvec.*Varvec;
      D(c).pmod(3).poly = 1;

      c = c+1;

      % Choice dummy contingency
      dummy_vec = strmatch('Dummy',T.cond(1:56));
      if ~isempty(dummy_vec),
        D(c).name = 'Dummy_choice';
        D(c).onsets = T.Choice_started(dummy_vec) - T.LongITI1_started(1);
        D(c).durations = T.ChoiceDur(dummy_vec);
        c = c+1;
      end
      choice_size = size(D,2);

      % Outcome
      cond = unique(T.cond(1:56));
      for c = c:(c+size(cond,1)-1),
        % Out dummy contingency
        if strmatch('Dummy',cond{c-choice_size}),
          D(c).name = 'Dummy_out';
        else
          D(c).name = cond{c-choice_size};
        end
        D(c).onsets = T.Feedback_started(strmatch(cond{c-choice_size},T.cond(1:56))) - T.LongITI1_started(1);
        D(c).durations = T.Feedback_stopped(strmatch(cond{c-choice_size},T.cond(1:56))) - T.Feedback_started(strmatch(cond{c-choice_size},T.cond(1:56)));
        c = c+1;
      end
  end
end
