sourcedir = '/home/joshgoh/Projects/7t_josh/data/rawdata/240709JAM_EVA_LCT/beh/'
derivdir = '/home/joshgoh/Projects/7t_josh/data/derivatives/240709JAM_EVA_LCT/beh/'

import os
import pandas as pd
import numpy as np
from scipy.io import savemat

r = 1
f = []

for file in sorted(os.listdir(sourcedir)):
    if file.endswith('.csv'):
        f.append(file)

# Per session procedures (for generating soa files for SPM + other usages)
#df_global = pd.DataFrame()
for file in f:
    df = pd.read_csv(sourcedir+file)
    df = df.dropna(how='all')
    #df_global = pd.concat([df_global,df],axis=0)

    # Init
    ## By categories (for condition contrasts)
    cond_name = np.zeros((len(pd.unique(df.Label))+len(pd.unique(df.cond)),), dtype=object)
    cond_ons = np.zeros((len(pd.unique(df.Label))+len(pd.unique(df.cond)),), dtype=object)
    cond_dur = np.zeros((len(pd.unique(df.Label))+len(pd.unique(df.cond)),), dtype=object)
    ## By trials (for parametric models)
    trial_cond_name = np.zeros((1,), dtype=object)
    trial_cond_ons = np.zeros((1,), dtype=object)
    trial_cond_dur = np.zeros((1,), dtype=object)
    trial_cond_par = np.zeros((1,), dtype=object)
    k=0
    L = []

    # Choice
    trial_cond_name[0] = 'Choice'
    for i in pd.unique(df.Label):
        cond_name[k] = [i]
        temp = df['Choice.started'][df.Label == i]-df['LongITI1.started'][0]
        P = df['Win'][df.Label == i]
        M = df['Value'][df.Label == i]
        EV = (df['Win'][df.Label == i]*df['Value'][df.Label == i])+((1-df['Win'][df.Label == i])*-df['Value'][df.Label == i])
        #for p in ['Prob','Mag','EV']:
        #    trial_cond_par[p].extend()
        cond_ons[k] = np.reshape(temp.to_numpy(),(len(temp.to_numpy()),1))
        #trial_cond_ons.extend(cond_ons[k])
        cond_dur[k] = [3]
        #trial_cond_dur.extend(con_dur[k])
        L.extend([' Cond: '+i+'\n'])
        L.extend(['   Onsets: '+str(temp.to_numpy())+'\n'])
        L.extend(['   Durations: '+str([3])+'\n'])
        k+=1

    # Outcome
    trial_cond_name[1] = 'Outcome'
    for i in pd.unique(df.cond):
        cond_name[k] = [i]
        temp = df['Feedback.started'][df.cond == i]-df['LongITI1.started'][0]
        cond_ons[k] = np.reshape(temp.to_numpy(),(len(temp.to_numpy()),1))
        cond_dur[k] = [1.5]
        L.extend([' Cond: '+i+'\n'])
        L.extend(['   Onsets: '+str(temp.to_numpy())+'\n'])
        L.extend(['   Durations: '+str([1.5])+'\n'])
        k+=1

    mdict = {"names": cond_name, "onsets": cond_ons, "durations": dur_ons}

    # Export to .mat for SPM 1st-level
    savemat(pderivdir+'lct-sal-r'+str(r)+'_soa.mat', mdict)

    # Export to .yaml
    yaml = open(pderivdir+'lct-sal-r'+str(r)+'_soa.yaml','w+')
    yaml.write("---\n")
    yaml.writelines(L)
    yaml.close()

    r+=1
