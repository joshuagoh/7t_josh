% Matlab function to define LCT SPM mat contrasts.
% Usage: C = lct_1lvl_def_con(SPMmatfn,type,consign)
%
% C        - Structure containing condition names, onsets, durations, modulators.
% SPMmatfn - SPM.mat filename
% type     - 'Bin','Prob*Mag','EV'
% consign  - 'pos','neg','pos_neg'
%
% Created by Josh Goh 202409141646GMT+8.

function C = lct_1lvl_def_con(SPMmatfn,type,consign)
  load(SPMmatfn);
  if strcmp(type,'Bin'),
    con = {'ProbHH_valueH';
           'ProbHH_valueL';
           'ProbLL_valueH';
           'ProbLL_valueL';
           'ProbMM_valueH';
           'ProbMM_valueL';
           'ProbMM_valueSH';
           'A_GAIN';
           'A_LOSS';
           'R_GAIN';
           'R_LOSS'};

    % T contrasts
    j = 1;
    for i = 1:size(con,1)
      if contains(consign,'pos'),
        C(j).type = 'T';
        C(j).name = con{i};
        C(j).con = contains(SPM.xX.name,con{i})*1;
        j = j+1;
      end
      if contains(consign,'neg'),
        C(j).type = 'T';
        C(j).name = ['-' con{i}];
        C(j).con = contains(SPM.xX.name,con{i})*-1;
        j = j+1;
      end
    end

    % F contrasts
    C(j).type = 'F';
    C(j).name = 'ALL';
    C(j).con = [];
    for k = 1:size(con,1);
      C(j).con = [C(j).con;contains(SPM.xX.name,con{k})*1];
    end
  end
  if strcmp(type,'Prob_Mag'),
    con = {'ChoicexProb^1';
           'ChoicexProb2^1';
           'ChoicexMag^1';
           'ChoicexProb:Mag^1';
           'ChoicexProb2:Mag^1'};

    % T contrasts
    j = 1;
    for i = 1:size(con,1)
      if contains(consign,'pos'),
        C(j).type = 'T';
        C(j).name = con{i};
        C(j).con = contains(SPM.xX.name,con{i})*1;
        j = j+1;
      end
      if contains(consign,'neg'),
        C(j).type = 'T';
        C(j).name = ['-' con{i}];
        C(j).con = contains(SPM.xX.name,con{i})*-1;
        j = j+1;
      end
    end

    % F contrasts
    C(j).type = 'F';
    C(j).name = 'ALL';
    C(j).con = [];
    for k = 1:size(con,1);
      C(j).con = [C(j).con;contains(SPM.xX.name,con{k})*1];
    end
  end
  if strcmp(type,'EV'),
    con = {'ChoicexEV^1';
           'ChoicexVar^1';
           'ChoicexEV:Variance^1'};

    % T contrasts
    j = 1;
    for i = 1:size(con,1)
      if contains(consign,'pos'),
        C(j).type = 'T';
        C(j).name = con{i};
        C(j).con = contains(SPM.xX.name,con{i})*1;
        j = j+1;
      end
      if contains(consign,'neg'),
        C(j).type = 'T';
        C(j).name = ['-' con{i}];
        C(j).con = contains(SPM.xX.name,con{i})*-1;
        j = j+1;
      end
    end

    % F contrasts
    C(j).type = 'F';
    C(j).name = 'ALL';
    C(j).con = [];
    for k = 1:size(con,1);
      C(j).con = [C(j).con;contains(SPM.xX.name,con{k})*1];
    end
  end
end
