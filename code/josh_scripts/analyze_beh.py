sourcedir = '/home/joshgoh/Projects/7t_josh/data/rawdata/240709JAM_EVA_LCT/beh/'
derivdir = '/home/joshgoh/Projects/7t_josh/data/rawdata/240709JAM_EVA_LCT/beh/'

import os
import pandas as pd
import numpy as np
from scipy.io import savemat

r = 1
f = []

for file in sorted(os.listdir(sourcedir)):
    if file.endswith('.csv'):
        f.append(file)

# Per session procedures (for generating soa files for SPM + other usages)
df_global = pd.DataFrame()
for file in f:
    df = pd.read_csv(sourcedir+file)
    df = df.dropna(how='all')
    df_global = pd.concat([df_global,df],axis=0)

    # Init
    ## By categories (for condition contrasts)
    cond_name = np.zeros((len(pd.unique(df.Label))+len(pd.unique(df.cond)),), dtype=object)
    cond_ons = np.zeros((len(pd.unique(df.Label))+len(pd.unique(df.cond)),), dtype=object)
    cond_dur = np.zeros((len(pd.unique(df.Label))+len(pd.unique(df.cond)),), dtype=object)
    ## By trials (for parametric models)
    trial_cond_name = np.zeros((1,), dtype=object)
    trial_cond_ons = np.zeros((1,), dtype=object)
    trial_cond_dur = np.zeros((1,), dtype=object)
    trial_cond_par = np.zeros((1,), dtype=object)
    k=0
    L = []

    # Choice
    trial_cond_name[0] = 'Choice'
    for i in pd.unique(df.Label):
        cond_name[k] = [i]
        temp = df['Choice.started'][df.Label == i]-df['LongITI1.started'][0]
        P = df['Win'][df.Label == i]
        M = df['Value'][df.Label == i]
        EV = (df['Win'][df.Label == i]*df['Value'][df.Label == i])+((1-df['Win'][df.Label == i])*-df['Value'][df.Label == i])
        #for p in ['Prob','Mag','EV']:
        #    trial_cond_par[p].extend()
        cond_ons[k] = np.reshape(temp.to_numpy(),(len(temp.to_numpy()),1))
        trial_cond_ons.extend(cond_ons[k])
        cond_dur[k] = [3]
        trial_cond_dur.extend(con_dur[k])
        L.extend([' Cond: '+i+'\n'])
        L.extend(['   Onsets: '+str(temp.to_numpy())+'\n'])
        L.extend(['   Durations: '+str([3])+'\n'])
        k+=1

    # Outcome
    trial_cond_name[1] = 'Outcome'
    for i in pd.unique(df.cond):
        cond_name[k] = [i]
        temp = df['Feedback.started'][df.cond == i]-df['LongITI1.started'][0]
        cond_ons[k] = np.reshape(temp.to_numpy(),(len(temp.to_numpy()),1))
        cond_dur[k] = [1.5]
        L.extend([' Cond: '+i+'\n'])
        L.extend(['   Onsets: '+str(temp.to_numpy())+'\n'])
        L.extend(['   Durations: '+str([1.5])+'\n'])
        k+=1

    mdict = {"names": cond_name, "onsets": cond_ons, "durations": dur_ons}

    # Export to .mat for SPM 1st-level
    savemat(pderivdir+'lct-sal-r'+str(r)+'_soa.mat', mdict)

    # Export to .yaml
    yaml = open(pderivdir+'lct-sal-r'+str(r)+'_soa.yaml','w+')
    yaml.write("---\n")
    yaml.writelines(L)
    yaml.close()

    r+=1

# Global (across session) analyses of responses
## Derive additional predictors: Prob, Mag, EV, delta_zero, delta
df_global = df_global.iloc[:,[0,1,2,3,4,32,33,39,40,46,56,58]]
P = df_global.Win/100
M = df_global.Value
EV = (P*M)+((1-P)*(-M))
df_global.insert(3, "EV", EV, True)
df_global.insert(4,"Prob",df_global.Label.str.split("_",expand=True).get(0),True)
df_global.insert(5,"Mag",df_global.Label.str.split("_",expand=True).get(1),True)
df_global.loc[df_global['choice_key.keys']==2.0,'choice_key.keys']=-1
df_global.insert(11,"delta",df_global[['choice_key.keys']].iloc[:,0]*df_global[['RealValue']].iloc[:,0],True)
df_global.loc[df_global['choice_key.keys']==-1,'choice_key.keys']=0
df_global.insert(12,"delta_zero",df_global[['choice_key.keys']].iloc[:,0]*df_global[['RealValue']].iloc[:,0],True)
df_global.to_csv('LCT_beh.csv', index=False)

## Category effects
choice_categories = pd.unique(df_global.Label)
acceptance_rates = np.zeros((choice_categories.shape), dtype=object)
acceptance_rts = np.zeros((choice_categories.shape), dtype=object)
rejection_rts = np.zeros((choice_categories.shape), dtype=object)

for i in np.:
    choice_key = df_global.loc[df_global['Label']==choice_categories[i],'choice_key.keys']
    acceptance_rates[i] = np.nansum(choice_key)/np.count_nonzero(~np.isnan(choice_key))
    acceptance_rts = np.nanmean(df_global.loc[(df_global['Label']==choice_categories[i]) & (df_global['choice_key.keys']==1),'choice_key.rt'])
    rejection_rts = np.nanmean(df_global.loc[(df_global['Label']==choice_categories[i]) & (df_global['choice_key.keys']==0),'choice_key.rt'])

import statsmodels.api as sm
import statsmodels.formula.api as smf

df_global = df_global.rename(columns={"choice_key.rt": "rt", "choice_key.keys": "keys"})
md = smf.mixedlm("rt ~ EV", df_global, groups=df_global["Label"])
mdf = md.fit()
print(mdf.summary())

import plotly.graph_objects as go
fig = go.Figure(data=go.Scatter(x=df_global["EV"], y=df_global["rt"], mode='markers'))
fig.show()


# Export individual behavioral summary line (for external group analysis)
