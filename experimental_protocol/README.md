# Different experimental tasks for testing with VAPER@7T.

- **spatial-navigation-task**: Maze navigation experiment programed in Unity3D. Executables for this protocol are on the stimuli presentation machines and not in this online repo due to size limitations. 

- **ACTI**: Active inference task programmed in PsychoPy. Developed for probing inferential processing in the Lego Robot Programming intervention clinical trial [NCT05341232](https://classic.clinicaltrials.gov/ct2/show/NCT05341232).

- **LCT_en**: Lottery Choice Task (English) programmed in PsychoPy. Developed for probing value-based processing system.