# Spatial Navigation with Agency vs. Tour option
- There are two maps: A & B and two conditions: FREE & TOUR. The protocol requires E-Prime 2.0 running on a Windows OS.

- Stimuli and e-prime files are hosted on sftp://140.112.121.218. Contact joshuagoh@ntu.edu.tw for access. Only basic practice files and markdown files are stored on the gitlab repository.

- For counterbalancing of maps and conditions, an example participant is designated to play A/TOUR first and then B/FREE. The procedural sequence for this participant is as follows:

## ON THE FIRST DAY/SESSION
### Orientation
1. TargetA/!testTargetA.ebs2 (Familiarize with landmarks to memorize; if the participant is designated to play B first, change to "!testTargetB.ebs2" instead)
2. Practice/PTour.ebs2 (Familiarize with TOUR environment; if the participant is designated to play FREE first, change to "PFree.ebs2" instead)
3. Practice/PTask.ebs2 (Familiarize with retrieval procedure; REPEAT UNTIL PARTICIPANTS CAN SMOOTHLY COMPLETE THIS TASK)
### Formal experiment
4. A/ATour1.ebs2 (~240 sec)
5. A/ATour2.ebs2
6. A/ATour3.ebs2
7. A/ATour4.ebs2
8. A/ATask1.ebs2 (Maximal 540 sec)
9. A/ATask2.ebs2
10. A/ATask3.ebs2
11. A/ATask4.ebs2

## ON THE SECOND DAY/
### Orientation
1. TargetB/!testTargetB.ebs2 (Familiarize with landmarks to memorize; if the participant is designated to play A second, change to "!testTargetA.ebs2" instead)
2. Practice/PFree.ebs2 (Familiarize with FREE environment; if the participant is designated to play TOUR second, change to "PTour.ebs2" instead)
3. Practice/PTask.ebs2 (Familiarize with retrieval procedure; REPEAT UNTIL PARTICIPANTS CAN SMOOTHLY COMPLETE THIS TASK)
### Formal experiment
4. B/BFree1.ebs2 (~240 sec)
5. B/BFree2.ebs2
6. B/BFree3.ebs2
7. B/BFree4.ebs2
8. B/BTask1.ebs2 (Maximal 540 sec)
9. B/BTask2.ebs2
10. B/BTask3.ebs2
11. B/BTask4.ebs2

*A caveat though, is that the landmarks in the experiment are Taiwanese shops, so they may contain Mandarin characters and may not be familiar to participants outside Taiwan.*
