# Spatial Navigation Task - simplified
- This stimuli presentation program was created using Unity3D and requires a Windows OS to run the .exe program. The files are hosted on sftp://140.112.121.218. Contact joshuagoh@ntu.edu.tw for access. Only basic practice files and markdown files are stored on the gitlab repository.

- The virtual environment employed in this experimental protocol is a simplified version of the one in [SNT-agency](experimental_protocol/SNT/SNT-agency). Notably, the graphical textures are drastically reduced in visual complexity, the paths are simplified, and the protocol only does free navigation encoding and retrieval.

## Current versions
- SNT_win_202405081832
  - Original version with minimal changes.
- SNT_win_202405151902
  - Learning phase in English with "=" trigger setting.
- SNT_win_202405211218CDT (active)
  - Major enhancement. Full English (learning, testing), replaced with US version of shops, "=" trigger setting, longer "+" fixation in between testing trials, timing extensions for testing judgment phase, better flow of runs, clearer menu.