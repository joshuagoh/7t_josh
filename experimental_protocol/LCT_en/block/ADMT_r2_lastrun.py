﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v3.1.5),
    on 八月 13, 2019, at 16:19
If you publish work using this script please cite the PsychoPy publications:
    Peirce, JW (2007) PsychoPy - Psychophysics software in Python.
        Journal of Neuroscience Methods, 162(1-2), 8-13.
    Peirce, JW (2009) Generating stimuli for neuroscience using PsychoPy.
        Frontiers in Neuroinformatics, 2:10. doi: 10.3389/neuro.11.010.2008
"""

from __future__ import absolute_import, division
from psychopy import locale_setup, sound, gui, visual, core, data, event, logging, clock
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)
import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard

# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '3.1.5'
expName = 'ADMT_r2'  # from the Builder filename that created this script
expInfo = {'participant': '', 'session': '001', 'starting_score': ''}
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/%s_%s_%s' % (expInfo['participant'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='D:\\ADMT_task\\ADM_fMRI\\ADM_fMRI_en\\ADMT_r2_lastrun.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp

# Start Code - component code to be run before the window creation

# Setup the Window
win = visual.Window(
    size=[1920, 1080], fullscr=True, screen=0, 
    winType='pyglet', allowGUI=False, allowStencil=False,
    monitor='testMonitor', color=[-1.000,-1.000,-1.000], colorSpace='rgb',
    blendMode='avg', useFBO=True, 
    units='height')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard()

# Initialize components for Routine "Welcome"
WelcomeClock = core.Clock()
welcome = visual.TextStim(win=win, name='welcome',
    text='                    Welcome to the experiment!\n\nToday you will be playing a points-accumulation game.\nYour goal is to accumulate as many points as you can.\n\n                    (Press left button to continue)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Review1"
Review1Clock = core.Clock()
review1 = visual.TextStim(win=win, name='review1',
    text='              The display will tell you the number \nof points at stake and the likelihood of a win and a loss.\n\n            If you accept, you may gain or you may \n              lose the displayed amount of points.\n\n            If you decline, you do not lose points,\n               but cannot boost your score either.\n\n                (Press right button to continue)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Review2"
Review2Clock = core.Clock()
review2 = visual.TextStim(win=win, name='review2',
    text='                                        Remember：\n\nWhen the game starts, the display will show a cross in the center.\n                Please maintain your attention on the cross.\n\n                            When an offer is displayed:\n                    If you accept, press the LEFT button,\n                   If you decline, press the RIGHT button.\n                 Respond as fast as you can for each offer.\n                After each offer, the outcome will be shown.\n\n                                          Get ready!\n                                (press any key to start)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "ScannerPulse"
ScannerPulseClock = core.Clock()
scannerpulse = visual.TextStim(win=win, name='scannerpulse',
    text='+',
    font='Arial',
    units='norm', pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "LongITI1"
LongITI1Clock = core.Clock()
longiti1 = visual.TextStim(win=win, name='longiti1',
    text='+',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Choice"
ChoiceClock = core.Clock()
totalsum = int(expInfo['starting_score'])
choice = visual.TextStim(win=win, name='choice',
    text='default text',
    font='Arial',
    units='norm', pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);

# Initialize components for Routine "ITI1"
ITI1Clock = core.Clock()
iti1 = visual.TextStim(win=win, name='iti1',
    text='+',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Feedback"
FeedbackClock = core.Clock()
feedback = visual.TextStim(win=win, name='feedback',
    text='default text',
    font='Arial',
    units='norm', pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);

# Initialize components for Routine "ITI2"
ITI2Clock = core.Clock()
iti2 = visual.TextStim(win=win, name='iti2',
    text='+',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "LongITI2"
LongITI2Clock = core.Clock()
longiti2 = visual.TextStim(win=win, name='longiti2',
    text='+',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "BreakWindow"
BreakWindowClock = core.Clock()
text = visual.TextStim(win=win, name='text',
    text='default text',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "Welcome"-------
t = 0
WelcomeClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp = keyboard.Keyboard()
# keep track of which components have finished
WelcomeComponents = [welcome, key_resp]
for thisComponent in WelcomeComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Welcome"-------
while continueRoutine:
    # get current time
    t = WelcomeClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *welcome* updates
    if t >= 0.0 and welcome.status == NOT_STARTED:
        # keep track of start time/frame for later
        welcome.tStart = t  # not accounting for scr refresh
        welcome.frameNStart = frameN  # exact frame index
        win.timeOnFlip(welcome, 'tStartRefresh')  # time at next scr refresh
        welcome.setAutoDraw(True)
    
    # *key_resp* updates
    if t >= 0.0 and key_resp.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp.tStart = t  # not accounting for scr refresh
        key_resp.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp, 'tStartRefresh')  # time at next scr refresh
        key_resp.status = STARTED
        # keyboard checking is just starting
        key_resp.clearEvents(eventType='keyboard')
    if key_resp.status == STARTED:
        theseKeys = key_resp.getKeys(keyList=['1'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in WelcomeComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Welcome"-------
for thisComponent in WelcomeComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "Welcome" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Review1"-------
t = 0
Review1Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_2 = keyboard.Keyboard()
# keep track of which components have finished
Review1Components = [review1, key_resp_2]
for thisComponent in Review1Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Review1"-------
while continueRoutine:
    # get current time
    t = Review1Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *review1* updates
    if t >= 0.0 and review1.status == NOT_STARTED:
        # keep track of start time/frame for later
        review1.tStart = t  # not accounting for scr refresh
        review1.frameNStart = frameN  # exact frame index
        win.timeOnFlip(review1, 'tStartRefresh')  # time at next scr refresh
        review1.setAutoDraw(True)
    
    # *key_resp_2* updates
    if t >= 0.0 and key_resp_2.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_2.tStart = t  # not accounting for scr refresh
        key_resp_2.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_2, 'tStartRefresh')  # time at next scr refresh
        key_resp_2.status = STARTED
        # keyboard checking is just starting
        key_resp_2.clearEvents(eventType='keyboard')
    if key_resp_2.status == STARTED:
        theseKeys = key_resp_2.getKeys(keyList=['2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Review1Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Review1"-------
for thisComponent in Review1Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "Review1" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Review2"-------
t = 0
Review2Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_3 = keyboard.Keyboard()
# keep track of which components have finished
Review2Components = [review2, key_resp_3]
for thisComponent in Review2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Review2"-------
while continueRoutine:
    # get current time
    t = Review2Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *review2* updates
    if t >= 0.0 and review2.status == NOT_STARTED:
        # keep track of start time/frame for later
        review2.tStart = t  # not accounting for scr refresh
        review2.frameNStart = frameN  # exact frame index
        win.timeOnFlip(review2, 'tStartRefresh')  # time at next scr refresh
        review2.setAutoDraw(True)
    
    # *key_resp_3* updates
    if t >= 0.0 and key_resp_3.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_3.tStart = t  # not accounting for scr refresh
        key_resp_3.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_3, 'tStartRefresh')  # time at next scr refresh
        key_resp_3.status = STARTED
        # keyboard checking is just starting
        key_resp_3.clearEvents(eventType='keyboard')
    if key_resp_3.status == STARTED:
        theseKeys = key_resp_3.getKeys(keyList=['1', '2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Review2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Review2"-------
for thisComponent in Review2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "Review2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "ScannerPulse"-------
t = 0
ScannerPulseClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_4 = keyboard.Keyboard()
# keep track of which components have finished
ScannerPulseComponents = [scannerpulse, key_resp_4]
for thisComponent in ScannerPulseComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "ScannerPulse"-------
while continueRoutine:
    # get current time
    t = ScannerPulseClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *scannerpulse* updates
    if t >= 0.0 and scannerpulse.status == NOT_STARTED:
        # keep track of start time/frame for later
        scannerpulse.tStart = t  # not accounting for scr refresh
        scannerpulse.frameNStart = frameN  # exact frame index
        win.timeOnFlip(scannerpulse, 'tStartRefresh')  # time at next scr refresh
        scannerpulse.setAutoDraw(True)
    
    # *key_resp_4* updates
    if t >= 0.0 and key_resp_4.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_4.tStart = t  # not accounting for scr refresh
        key_resp_4.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_4, 'tStartRefresh')  # time at next scr refresh
        key_resp_4.status = STARTED
        # keyboard checking is just starting
        key_resp_4.clearEvents(eventType='keyboard')
    if key_resp_4.status == STARTED:
        theseKeys = key_resp_4.getKeys(keyList=['5'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in ScannerPulseComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "ScannerPulse"-------
for thisComponent in ScannerPulseComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "ScannerPulse" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "LongITI1"-------
t = 0
LongITI1Clock.reset()  # clock
frameN = -1
continueRoutine = True
routineTimer.add(20.000000)
# update component parameters for each repeat
# keep track of which components have finished
LongITI1Components = [longiti1]
for thisComponent in LongITI1Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "LongITI1"-------
while continueRoutine and routineTimer.getTime() > 0:
    # get current time
    t = LongITI1Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *longiti1* updates
    if t >= 0.0 and longiti1.status == NOT_STARTED:
        # keep track of start time/frame for later
        longiti1.tStart = t  # not accounting for scr refresh
        longiti1.frameNStart = frameN  # exact frame index
        win.timeOnFlip(longiti1, 'tStartRefresh')  # time at next scr refresh
        longiti1.setAutoDraw(True)
    frameRemains = 0.0 + 20.0- win.monitorFramePeriod * 0.75  # most of one frame period left
    if longiti1.status == STARTED and t >= frameRemains:
        # keep track of stop time/frame for later
        longiti1.tStop = t  # not accounting for scr refresh
        longiti1.frameNStop = frameN  # exact frame index
        win.timeOnFlip(longiti1, 'tStopRefresh')  # time at next scr refresh
        longiti1.setAutoDraw(False)
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in LongITI1Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "LongITI1"-------
for thisComponent in LongITI1Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('longiti1.started', longiti1.tStartRefresh)
thisExp.addData('longiti1.stopped', longiti1.tStopRefresh)

# set up handler to look after randomisation of conditions etc
trials = data.TrialHandler(nReps=1, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('BML_r2.xlsx'),
    seed=None, name='trials')
thisExp.addLoop(trials)  # add the loop to the experiment
thisTrial = trials.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
if thisTrial != None:
    for paramName in thisTrial:
        exec('{} = thisTrial[paramName]'.format(paramName))

for thisTrial in trials:
    currentLoop = trials
    # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
    if thisTrial != None:
        for paramName in thisTrial:
            exec('{} = thisTrial[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "Choice"-------
    t = 0
    ChoiceClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(4.000000)
    # update component parameters for each repeat
    Choice_txt = '          {} points\n\n{}% win      {}% lose\n\n  Accept         Decline  '.format(Value,Win,Lose)
    choice.setText(Choice_txt)
    choice_key = keyboard.Keyboard()
    # keep track of which components have finished
    ChoiceComponents = [choice, choice_key]
    for thisComponent in ChoiceComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "Choice"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = ChoiceClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *choice* updates
        if t >= 0.0 and choice.status == NOT_STARTED:
            # keep track of start time/frame for later
            choice.tStart = t  # not accounting for scr refresh
            choice.frameNStart = frameN  # exact frame index
            win.timeOnFlip(choice, 'tStartRefresh')  # time at next scr refresh
            choice.setAutoDraw(True)
        frameRemains = 0.0 + 4.0- win.monitorFramePeriod * 0.75  # most of one frame period left
        if choice.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            choice.tStop = t  # not accounting for scr refresh
            choice.frameNStop = frameN  # exact frame index
            win.timeOnFlip(choice, 'tStopRefresh')  # time at next scr refresh
            choice.setAutoDraw(False)
        
        # *choice_key* updates
        if t >= 0.0 and choice_key.status == NOT_STARTED:
            # keep track of start time/frame for later
            choice_key.tStart = t  # not accounting for scr refresh
            choice_key.frameNStart = frameN  # exact frame index
            win.timeOnFlip(choice_key, 'tStartRefresh')  # time at next scr refresh
            choice_key.status = STARTED
            # keyboard checking is just starting
            win.callOnFlip(choice_key.clock.reset)  # t=0 on next screen flip
            choice_key.clearEvents(eventType='keyboard')
        frameRemains = 0.0 + 4.0- win.monitorFramePeriod * 0.75  # most of one frame period left
        if choice_key.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            choice_key.tStop = t  # not accounting for scr refresh
            choice_key.frameNStop = frameN  # exact frame index
            win.timeOnFlip(choice_key, 'tStopRefresh')  # time at next scr refresh
            choice_key.status = FINISHED
        if choice_key.status == STARTED:
            theseKeys = choice_key.getKeys(keyList=['1', '2'], waitRelease=False)
            if len(theseKeys):
                theseKeys = theseKeys[0]  # at least one key was pressed
                
                # check for quit:
                if "escape" == theseKeys:
                    endExpNow = True
                if choice_key.keys == []:  # then this was the first keypress
                    choice_key.keys = theseKeys.name  # just the first key pressed
                    choice_key.rt = theseKeys.rt
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in ChoiceComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Choice"-------
    for thisComponent in ChoiceComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    trials.addData('choice.started', choice.tStartRefresh)
    trials.addData('choice.stopped', choice.tStopRefresh)
    # check responses
    if choice_key.keys in ['', [], None]:  # No response was made
        choice_key.keys = None
    trials.addData('choice_key.keys',choice_key.keys)
    if choice_key.keys != None:  # we had a response
        trials.addData('choice_key.rt', choice_key.rt)
    trials.addData('choice_key.started', choice_key.tStartRefresh)
    trials.addData('choice_key.stopped', choice_key.tStopRefresh)
    theseKeys = choice_key.keys
    
    if theseKeys == '1':
        LBracket = ""
        RBracket = ""
        if RealValue > 0:
            Gainsign = '+'
        else:
            Gainsign = ''
        Outcome = RealValue
        RespCheck = ""
        totalsum = totalsum + Outcome
    elif theseKeys == '2':
        LBracket = "("
        RBracket = ")"
        if RealValue > 0:
            Gainsign = '+'
        else:
            Gainsign = ''
        Outcome = RealValue
        RespCheck = ""
        totalsum = totalsum
    elif theseKeys == None:
        LBracket = ""
        RBracket = ""
        Gainsign = ""
        Outcome = 0
        RespCheck = "Please respond!!!"
        totalsum = totalsum
    
    thisExp.addData('LBracket', LBracket)
    thisExp.addData('RBracket', RBracket)
    thisExp.addData('Gainsign', Gainsign)
    thisExp.addData('RespCheck', RespCheck)
    thisExp.addData('Outcome', Outcome)
    thisExp.addData('totalsum', totalsum)
    
    
    # ------Prepare to start Routine "ITI1"-------
    t = 0
    ITI1Clock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    # keep track of which components have finished
    ITI1Components = [iti1]
    for thisComponent in ITI1Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "ITI1"-------
    while continueRoutine:
        # get current time
        t = ITI1Clock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *iti1* updates
        if t >= 0.0 and iti1.status == NOT_STARTED:
            # keep track of start time/frame for later
            iti1.tStart = t  # not accounting for scr refresh
            iti1.frameNStart = frameN  # exact frame index
            win.timeOnFlip(iti1, 'tStartRefresh')  # time at next scr refresh
            iti1.setAutoDraw(True)
        frameRemains = 0.0 + ITIDur1- win.monitorFramePeriod * 0.75  # most of one frame period left
        if iti1.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            iti1.tStop = t  # not accounting for scr refresh
            iti1.frameNStop = frameN  # exact frame index
            win.timeOnFlip(iti1, 'tStopRefresh')  # time at next scr refresh
            iti1.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in ITI1Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "ITI1"-------
    for thisComponent in ITI1Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # the Routine "ITI1" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # ------Prepare to start Routine "Feedback"-------
    t = 0
    FeedbackClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(2.000000)
    # update component parameters for each repeat
    feedback_txt = '     {}{}{}{} points\n {}\n\n total {} points'.format(LBracket,Gainsign,Outcome,RBracket,RespCheck,totalsum)
    feedback.setText(feedback_txt)
    # keep track of which components have finished
    FeedbackComponents = [feedback]
    for thisComponent in FeedbackComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "Feedback"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = FeedbackClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *feedback* updates
        if t >= 0.0 and feedback.status == NOT_STARTED:
            # keep track of start time/frame for later
            feedback.tStart = t  # not accounting for scr refresh
            feedback.frameNStart = frameN  # exact frame index
            win.timeOnFlip(feedback, 'tStartRefresh')  # time at next scr refresh
            feedback.setAutoDraw(True)
        frameRemains = 0.0 + 2.0- win.monitorFramePeriod * 0.75  # most of one frame period left
        if feedback.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            feedback.tStop = t  # not accounting for scr refresh
            feedback.frameNStop = frameN  # exact frame index
            win.timeOnFlip(feedback, 'tStopRefresh')  # time at next scr refresh
            feedback.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in FeedbackComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Feedback"-------
    for thisComponent in FeedbackComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    trials.addData('feedback.started', feedback.tStartRefresh)
    trials.addData('feedback.stopped', feedback.tStopRefresh)
    if choice_key.keys == '1':
        if RealValue > 0:
            cond = 'A_GAIN'
        elif RealValue < 0:
            cond = 'A_LOSS'
    elif choice_key.keys == '2':
        if RealValue > 0:
            cond = 'R_GAIN'
        elif RealValue < 0:
            cond = 'R_LOSS'
    elif choice_key.keys == None:
        cond = 'Dummy'
    
    thisExp.addData('cond', cond)
    
    
    # ------Prepare to start Routine "ITI2"-------
    t = 0
    ITI2Clock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    # keep track of which components have finished
    ITI2Components = [iti2]
    for thisComponent in ITI2Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "ITI2"-------
    while continueRoutine:
        # get current time
        t = ITI2Clock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *iti2* updates
        if t >= 0.0 and iti2.status == NOT_STARTED:
            # keep track of start time/frame for later
            iti2.tStart = t  # not accounting for scr refresh
            iti2.frameNStart = frameN  # exact frame index
            win.timeOnFlip(iti2, 'tStartRefresh')  # time at next scr refresh
            iti2.setAutoDraw(True)
        frameRemains = 0.0 + ITIDur2- win.monitorFramePeriod * 0.75  # most of one frame period left
        if iti2.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            iti2.tStop = t  # not accounting for scr refresh
            iti2.frameNStop = frameN  # exact frame index
            win.timeOnFlip(iti2, 'tStopRefresh')  # time at next scr refresh
            iti2.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in ITI2Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "ITI2"-------
    for thisComponent in ITI2Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # the Routine "ITI2" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed 1 repeats of 'trials'


# ------Prepare to start Routine "LongITI2"-------
t = 0
LongITI2Clock.reset()  # clock
frameN = -1
continueRoutine = True
routineTimer.add(20.000000)
# update component parameters for each repeat
# keep track of which components have finished
LongITI2Components = [longiti2]
for thisComponent in LongITI2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "LongITI2"-------
while continueRoutine and routineTimer.getTime() > 0:
    # get current time
    t = LongITI2Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *longiti2* updates
    if t >= 0.0 and longiti2.status == NOT_STARTED:
        # keep track of start time/frame for later
        longiti2.tStart = t  # not accounting for scr refresh
        longiti2.frameNStart = frameN  # exact frame index
        win.timeOnFlip(longiti2, 'tStartRefresh')  # time at next scr refresh
        longiti2.setAutoDraw(True)
    frameRemains = 0.0 + 20.0- win.monitorFramePeriod * 0.75  # most of one frame period left
    if longiti2.status == STARTED and t >= frameRemains:
        # keep track of stop time/frame for later
        longiti2.tStop = t  # not accounting for scr refresh
        longiti2.frameNStop = frameN  # exact frame index
        win.timeOnFlip(longiti2, 'tStopRefresh')  # time at next scr refresh
        longiti2.setAutoDraw(False)
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in LongITI2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "LongITI2"-------
for thisComponent in LongITI2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)

# ------Prepare to start Routine "BreakWindow"-------
t = 0
BreakWindowClock.reset()  # clock
frameN = -1
continueRoutine = True
routineTimer.add(4.000000)
# update component parameters for each repeat
score = totalsum
text.setText('You are done with the second part! \nSo far you have collected {} points.\n\nWe will now take a short break.'.format(score))
# keep track of which components have finished
BreakWindowComponents = [text]
for thisComponent in BreakWindowComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "BreakWindow"-------
while continueRoutine and routineTimer.getTime() > 0:
    # get current time
    t = BreakWindowClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text* updates
    if t >= 0.0 and text.status == NOT_STARTED:
        # keep track of start time/frame for later
        text.tStart = t  # not accounting for scr refresh
        text.frameNStart = frameN  # exact frame index
        win.timeOnFlip(text, 'tStartRefresh')  # time at next scr refresh
        text.setAutoDraw(True)
    frameRemains = 0.0 + 4.0- win.monitorFramePeriod * 0.75  # most of one frame period left
    if text.status == STARTED and t >= frameRemains:
        # keep track of stop time/frame for later
        text.tStop = t  # not accounting for scr refresh
        text.frameNStop = frameN  # exact frame index
        win.timeOnFlip(text, 'tStopRefresh')  # time at next scr refresh
        text.setAutoDraw(False)
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in BreakWindowComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "BreakWindow"-------
for thisComponent in BreakWindowComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)

# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
