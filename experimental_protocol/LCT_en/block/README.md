# Lottery Choice Task (LCT) block design
These are the instructions on how to run the LCT block design files contained in this directory. These stimuli presentation files require [Psychopy](https://www.psychopy.org/) to be installed on your local system.

Current setting is as follows:
  - Choice phase 3.0 s
  - ITI1 0.1 s
  - Feedback phase 1.5 s
  - ITI2 0.2 s
  - IBI 20 s
  - 9 trials per block (4.8 s per trial, 43 s per block)
    - Onsets (s): 21 84 147 210 273
  - 5 blocks per run
  - 45 trials per run
  - 20 s fixation beginning
  - 10 s fixation end
  - Total time is per run 345 s
  - 4 runs

There are four experimental runs and one practice run in this protocol associated with the following files:

- ADMT_practice.psyexp
- ADMT_r1.psyexp
- ADMT_r2.psyexp
- ADMT_r3.psyexp
- ADMT_r4.psyexp

These .psyexp files call table values from the following spreadsheets:

- BML_practice1.xlsx
- BML_practice2.xlsx
- BML_r1.xlsx
- BML_r2.xlsx
- BML_r3.xlsx
- BML_r4.xlsx

The other files are scripts and log files generated when the stimuli presentation code in the psyexp files is run.

For **stimuli timing and sequence details**, please consult [LCT general README.md](experimental_protocol/LCT_en/README.md).

## To run
1. Open Psychopy.
2. Open the psyexp file you wish to run.
3. Click the play button (right pointing triangle).
4. The session info screen will appear for which you must enter the following:
  - **Participant ID code**. 9999 is an example for pilot participants.
  - **Session ID**. Leave default as 001 for first instance of the run. If run is repeated, you can enter 002 as necessary to avoid overwrite.
  - **Start points**. This is the total accumulated points that the participant has at the beginning of the run. If this is the first time running any run, then the start points are 0. If the participant has completed prior runs, you can note the last accumulated score from the latest run and enter it in this field. If the latest score is forgotten, you can open /data/*_ADMT_*.csv, scroll right and bottom to the 'totalsum' column to get the latest score.
