﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v3.1.5),
    on 八月 14, 2019, at 15:12
If you publish work using this script please cite the PsychoPy publications:
    Peirce, JW (2007) PsychoPy - Psychophysics software in Python.
        Journal of Neuroscience Methods, 162(1-2), 8-13.
    Peirce, JW (2009) Generating stimuli for neuroscience using PsychoPy.
        Frontiers in Neuroinformatics, 2:10. doi: 10.3389/neuro.11.010.2008
"""

from __future__ import absolute_import, division
from psychopy import locale_setup, sound, gui, visual, core, data, event, logging, clock
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)
import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard

# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '3.1.5'
expName = 'ADMT_practice'  # from the Builder filename that created this script
expInfo = {'participant': '', 'session': '001', 'starting_score': ''}
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/%s_%s_%s' % (expInfo['participant'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='D:\\ADMT_task\\ADM_fMRI\\ADM_fMRI_en\\ADMT_practice.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp

# Start Code - component code to be run before the window creation

# Setup the Window
win = visual.Window(
    size=[1920, 1080], fullscr=True, screen=0, 
    winType='pyglet', allowGUI=False, allowStencil=False,
    monitor='testMonitor', color=[-1.000,-1.000,-1.000], colorSpace='rgb',
    blendMode='avg', useFBO=True, 
    units='height')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard()

# Initialize components for Routine "Welcome"
WelcomeClock = core.Clock()
welcome = visual.TextStim(win=win, name='welcome',
    text='  Welcome to the experiment!\n\n\n(Press left button to continue)\n',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Instruction1"
Instruction1Clock = core.Clock()
instruction1 = visual.TextStim(win=win, name='instruction1',
    text='                  Welcome to the experiment!\n\n\nToday you will be playing a points-accumulation game.\n\nYour goal is to accumulate as many points as you can.\n\n\n                 (Press right button to continue)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Instruction2"
Instruction2Clock = core.Clock()
instruction2 = visual.TextStim(win=win, name='instruction2',
    text='     During each round you will see three numbers. \n                             For example:\n\n                                100 points\n\n                         93% win        7% lose\n\n                The top shows the points at stake. \n   The bottom two represent the likelihood of winning \n                         or losing the points.\n\n(If there are no questions, press left button to continue)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Instruction3"
Instruction3Clock = core.Clock()
instruction3 = visual.TextStim(win=win, name='instruction3',
    text='          The display also shows two alternatives: accept or decline.\n\n                                              100 points\n\n                                       93% win        7% lose\n\n                                       Accept            Decline\n\n   You need to decide whether you want to accept or decline the offer.\n\n        If you accept you have a chance to win the points at stake. \n        But there may also be a chance that you will lose the points. \nIf you decline, you will not lose any points, but will not gain any either.\n\n        (If there are no questions, press right button to continue)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Instruction4"
Instruction4Clock = core.Clock()
instruction4 = visual.TextStim(win=win, name='instruction4',
    text='The display also shows two alternatives: accept or decline.\n\n                                    100 points\n\n                            93% win        7% lose\n\n                            Accept            Decline\n\n                      If you wish to accept this offer,\n                            press the left button.\n                   And if you wish to decline this offer,\n                            press the right button.\n\n(If there are no questions, press left button to continue)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Instruction5"
Instruction5Clock = core.Clock()
instruction5 = visual.TextStim(win=win, name='instruction5',
    text='            In this example slide, if you accept the offer,\n\n                                    100 points\n\n                            93% win        7% lose\n\n                            Accept            Decline\n\n            There is a 93% chance of winning 100 points,\n                 with a 7% chance of losing 100 points.\n\n                            If you decline the offer,\n                   you will not gain or lose the points.\n\n(If there are no questions, press right button to continue)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "ExFeedback1"
ExFeedback1Clock = core.Clock()
exfeedback1 = visual.TextStim(win=win, name='exfeedback1',
    text='                After each round, the outcome will be shown.\n                You will see two more numbers. For example:\n\n                                        +100 points\n\n                                         567 points\n\n        If you accepted the offer, the points you won or lost will be \n shown as the top number. In the above example, you won 100 points.\n\n                The bottom number shows your running total score. \n        In the above example, 100 points were added to 467 points, \n                        so that your accumulated points are 567.\n\n      If you declined, you will see the actual outcome in parenthesis, \n                                for example: "( +100 ) points"\nThis indicates what the outcome would have been if you had accepted. \n           But there will be no change to the running total scores.\n\n                  (press left button to see second exmple)\n\n',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "ExDisplay2"
ExDisplay2Clock = core.Clock()
exdisplay2 = visual.TextStim(win=win, name='exdisplay2',
    text='            In this second example, 21 points are at stake.\n\n                                         21 points\n\n                                33% win        67% lose\n\n                                Accept            Decline\n\n    If you accept, there is a 33% chance of winning 21 points, \n                   with a 67% chance of losing 21 points.  \n\n    If you decline the offer, you will not gain or lose the points.\n\n        (If there are no questions, press right button to continue)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "ExChoice2"
ExChoice2Clock = core.Clock()
exchoice2 = visual.TextStim(win=win, name='exchoice2',
    text='                                     21 points\n\n                            33% win        67% lose\n\n                            Accept            Decline\n\n                        As indicated on the bottom,\n If you wish to accept, you should press the LEFT button,\nIf you wish to decline, you should press the RIGHT button.\n\n                        (press left button to continue)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "ExFeedback2"
ExFeedback2Clock = core.Clock()
exfeedback2 = visual.TextStim(win=win, name='exfeedback2',
    text='    After each round, the outcome will be shown. For example:\n\n                                         -21 points\n\n                                         287 points\n\n                                    If you accepted, \n  the points you won or lost will be shown as the top number. \n                    In this example, you lost 21 points.\n\n            If you declined, you will see "(-21) points" instead.\nYour running total scores will be shown as the bottom number.\n\n                        (press right button to continue)\n\n',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Instruction6"
Instruction6Clock = core.Clock()
instruction6 = visual.TextStim(win=win, name='instruction6',
    text='                                        Remember:\n\n                                    During each round,\n                      If you accept, press the LEFT button,\n                    If you decline, press the RIGHT button.\n\n        Then the display will show the points you won or lost.\nAnd the running total score will be shown as the bottom number.\n\n        (If there are no questions, press left button to continue)\n',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Instruction7"
Instruction7Clock = core.Clock()
instruction7 = visual.TextStim(win=win, name='instruction7',
    text='                                  Remember:\n\n  Your goal is to accumulate as many points as you can.\n\n        Please respond as fast as you can for each offer.\n\n  (If there are no questions, press left button to continue)',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "LetUsGo"
LetUsGoClock = core.Clock()
letusgo = visual.TextStim(win=win, name='letusgo',
    text='                            The practice is about to start.\n\n        As soon as you press any key, the computer will display \n                    a new screen with a cross in the center. \nPlease maintain your attention on the cross whenever it appears.\n\n            If there are no questions, press any key to start!',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Choice"
ChoiceClock = core.Clock()
totalsum = int(expInfo['starting_score'])
choice = visual.TextStim(win=win, name='choice',
    text='default text',
    font='Arial',
    units='norm', pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);

# Initialize components for Routine "ITI1"
ITI1Clock = core.Clock()
iti1 = visual.TextStim(win=win, name='iti1',
    text='+',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Feedback"
FeedbackClock = core.Clock()
feedback = visual.TextStim(win=win, name='feedback',
    text='default text',
    font='Arial',
    units='norm', pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);

# Initialize components for Routine "ITI2"
ITI2Clock = core.Clock()
iti2 = visual.TextStim(win=win, name='iti2',
    text='+',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "LetUsGo2"
LetUsGo2Clock = core.Clock()
letusgo2 = visual.TextStim(win=win, name='letusgo2',
    text='In the next practice, you will only have 4 seconds for each offer.\n\n        Therefore, you have to decide whether you want to \n          accept or decline within 4 seconds in this round.\n\n      If there are no questions, press any button to continue',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Choice2"
Choice2Clock = core.Clock()
choice2 = visual.TextStim(win=win, name='choice2',
    text='default text',
    font='Arial',
    units='norm', pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);

# Initialize components for Routine "ITI1"
ITI1Clock = core.Clock()
iti1 = visual.TextStim(win=win, name='iti1',
    text='+',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Feedback"
FeedbackClock = core.Clock()
feedback = visual.TextStim(win=win, name='feedback',
    text='default text',
    font='Arial',
    units='norm', pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);

# Initialize components for Routine "ITI2"
ITI2Clock = core.Clock()
iti2 = visual.TextStim(win=win, name='iti2',
    text='+',
    font='Arial',
    pos=(0, 0), height=0.1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "WhereToNow"
WhereToNowClock = core.Clock()
wheretonow = visual.TextStim(win=win, name='wheretonow',
    text='default text',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "Complete"
CompleteClock = core.Clock()
complete = visual.TextStim(win=win, name='complete',
    text='                            You have completed the practice! \n\nThe experimenter can answer the remaining questions you might have.\n\n                Next, we are going to conduct the formal session.\n\n                                            Good luck!',
    font='Arial',
    units='norm', pos=(0, 0), height=0.07, wrapWidth=10, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "Welcome"-------
t = 0
WelcomeClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp = keyboard.Keyboard()
# keep track of which components have finished
WelcomeComponents = [welcome, key_resp]
for thisComponent in WelcomeComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Welcome"-------
while continueRoutine:
    # get current time
    t = WelcomeClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *welcome* updates
    if t >= 0.0 and welcome.status == NOT_STARTED:
        # keep track of start time/frame for later
        welcome.tStart = t  # not accounting for scr refresh
        welcome.frameNStart = frameN  # exact frame index
        win.timeOnFlip(welcome, 'tStartRefresh')  # time at next scr refresh
        welcome.setAutoDraw(True)
    
    # *key_resp* updates
    if t >= 0.0 and key_resp.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp.tStart = t  # not accounting for scr refresh
        key_resp.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp, 'tStartRefresh')  # time at next scr refresh
        key_resp.status = STARTED
        # keyboard checking is just starting
        key_resp.clearEvents(eventType='keyboard')
    if key_resp.status == STARTED:
        theseKeys = key_resp.getKeys(keyList=['1'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in WelcomeComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Welcome"-------
for thisComponent in WelcomeComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "Welcome" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instruction1"-------
t = 0
Instruction1Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_2 = keyboard.Keyboard()
# keep track of which components have finished
Instruction1Components = [instruction1, key_resp_2]
for thisComponent in Instruction1Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Instruction1"-------
while continueRoutine:
    # get current time
    t = Instruction1Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instruction1* updates
    if t >= 0.0 and instruction1.status == NOT_STARTED:
        # keep track of start time/frame for later
        instruction1.tStart = t  # not accounting for scr refresh
        instruction1.frameNStart = frameN  # exact frame index
        win.timeOnFlip(instruction1, 'tStartRefresh')  # time at next scr refresh
        instruction1.setAutoDraw(True)
    
    # *key_resp_2* updates
    if t >= 0.0 and key_resp_2.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_2.tStart = t  # not accounting for scr refresh
        key_resp_2.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_2, 'tStartRefresh')  # time at next scr refresh
        key_resp_2.status = STARTED
        # keyboard checking is just starting
        key_resp_2.clearEvents(eventType='keyboard')
    if key_resp_2.status == STARTED:
        theseKeys = key_resp_2.getKeys(keyList=['2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instruction1Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instruction1"-------
for thisComponent in Instruction1Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "Instruction1" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instruction2"-------
t = 0
Instruction2Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_3 = keyboard.Keyboard()
# keep track of which components have finished
Instruction2Components = [instruction2, key_resp_3]
for thisComponent in Instruction2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Instruction2"-------
while continueRoutine:
    # get current time
    t = Instruction2Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instruction2* updates
    if t >= 0.0 and instruction2.status == NOT_STARTED:
        # keep track of start time/frame for later
        instruction2.tStart = t  # not accounting for scr refresh
        instruction2.frameNStart = frameN  # exact frame index
        win.timeOnFlip(instruction2, 'tStartRefresh')  # time at next scr refresh
        instruction2.setAutoDraw(True)
    
    # *key_resp_3* updates
    if t >= 0.0 and key_resp_3.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_3.tStart = t  # not accounting for scr refresh
        key_resp_3.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_3, 'tStartRefresh')  # time at next scr refresh
        key_resp_3.status = STARTED
        # keyboard checking is just starting
        key_resp_3.clearEvents(eventType='keyboard')
    if key_resp_3.status == STARTED:
        theseKeys = key_resp_3.getKeys(keyList=['1'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instruction2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instruction2"-------
for thisComponent in Instruction2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "Instruction2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instruction3"-------
t = 0
Instruction3Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_5 = keyboard.Keyboard()
# keep track of which components have finished
Instruction3Components = [instruction3, key_resp_5]
for thisComponent in Instruction3Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Instruction3"-------
while continueRoutine:
    # get current time
    t = Instruction3Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instruction3* updates
    if t >= 0.0 and instruction3.status == NOT_STARTED:
        # keep track of start time/frame for later
        instruction3.tStart = t  # not accounting for scr refresh
        instruction3.frameNStart = frameN  # exact frame index
        win.timeOnFlip(instruction3, 'tStartRefresh')  # time at next scr refresh
        instruction3.setAutoDraw(True)
    
    # *key_resp_5* updates
    if t >= 0.0 and key_resp_5.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_5.tStart = t  # not accounting for scr refresh
        key_resp_5.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_5, 'tStartRefresh')  # time at next scr refresh
        key_resp_5.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_5.clock.reset)  # t=0 on next screen flip
        key_resp_5.clearEvents(eventType='keyboard')
    if key_resp_5.status == STARTED:
        theseKeys = key_resp_5.getKeys(keyList=['2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_5.keys = theseKeys.name  # just the last key pressed
            key_resp_5.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instruction3Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instruction3"-------
for thisComponent in Instruction3Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_5.keys in ['', [], None]:  # No response was made
    key_resp_5.keys = None
thisExp.addData('key_resp_5.keys',key_resp_5.keys)
if key_resp_5.keys != None:  # we had a response
    thisExp.addData('key_resp_5.rt', key_resp_5.rt)
thisExp.nextEntry()
# the Routine "Instruction3" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instruction4"-------
t = 0
Instruction4Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_6 = keyboard.Keyboard()
# keep track of which components have finished
Instruction4Components = [instruction4, key_resp_6]
for thisComponent in Instruction4Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Instruction4"-------
while continueRoutine:
    # get current time
    t = Instruction4Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instruction4* updates
    if t >= 0.0 and instruction4.status == NOT_STARTED:
        # keep track of start time/frame for later
        instruction4.tStart = t  # not accounting for scr refresh
        instruction4.frameNStart = frameN  # exact frame index
        win.timeOnFlip(instruction4, 'tStartRefresh')  # time at next scr refresh
        instruction4.setAutoDraw(True)
    
    # *key_resp_6* updates
    if t >= 0.0 and key_resp_6.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_6.tStart = t  # not accounting for scr refresh
        key_resp_6.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_6, 'tStartRefresh')  # time at next scr refresh
        key_resp_6.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_6.clock.reset)  # t=0 on next screen flip
        key_resp_6.clearEvents(eventType='keyboard')
    if key_resp_6.status == STARTED:
        theseKeys = key_resp_6.getKeys(keyList=['1'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_6.keys = theseKeys.name  # just the last key pressed
            key_resp_6.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instruction4Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instruction4"-------
for thisComponent in Instruction4Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_6.keys in ['', [], None]:  # No response was made
    key_resp_6.keys = None
thisExp.addData('key_resp_6.keys',key_resp_6.keys)
if key_resp_6.keys != None:  # we had a response
    thisExp.addData('key_resp_6.rt', key_resp_6.rt)
thisExp.nextEntry()
# the Routine "Instruction4" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instruction5"-------
t = 0
Instruction5Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_7 = keyboard.Keyboard()
# keep track of which components have finished
Instruction5Components = [instruction5, key_resp_7]
for thisComponent in Instruction5Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Instruction5"-------
while continueRoutine:
    # get current time
    t = Instruction5Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instruction5* updates
    if t >= 0.0 and instruction5.status == NOT_STARTED:
        # keep track of start time/frame for later
        instruction5.tStart = t  # not accounting for scr refresh
        instruction5.frameNStart = frameN  # exact frame index
        win.timeOnFlip(instruction5, 'tStartRefresh')  # time at next scr refresh
        instruction5.setAutoDraw(True)
    
    # *key_resp_7* updates
    if t >= 0.0 and key_resp_7.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_7.tStart = t  # not accounting for scr refresh
        key_resp_7.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_7, 'tStartRefresh')  # time at next scr refresh
        key_resp_7.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_7.clock.reset)  # t=0 on next screen flip
        key_resp_7.clearEvents(eventType='keyboard')
    if key_resp_7.status == STARTED:
        theseKeys = key_resp_7.getKeys(keyList=['2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_7.keys = theseKeys.name  # just the last key pressed
            key_resp_7.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instruction5Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instruction5"-------
for thisComponent in Instruction5Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_7.keys in ['', [], None]:  # No response was made
    key_resp_7.keys = None
thisExp.addData('key_resp_7.keys',key_resp_7.keys)
if key_resp_7.keys != None:  # we had a response
    thisExp.addData('key_resp_7.rt', key_resp_7.rt)
thisExp.nextEntry()
# the Routine "Instruction5" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "ExFeedback1"-------
t = 0
ExFeedback1Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_8 = keyboard.Keyboard()
# keep track of which components have finished
ExFeedback1Components = [exfeedback1, key_resp_8]
for thisComponent in ExFeedback1Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "ExFeedback1"-------
while continueRoutine:
    # get current time
    t = ExFeedback1Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *exfeedback1* updates
    if t >= 0.0 and exfeedback1.status == NOT_STARTED:
        # keep track of start time/frame for later
        exfeedback1.tStart = t  # not accounting for scr refresh
        exfeedback1.frameNStart = frameN  # exact frame index
        win.timeOnFlip(exfeedback1, 'tStartRefresh')  # time at next scr refresh
        exfeedback1.setAutoDraw(True)
    
    # *key_resp_8* updates
    if t >= 0.0 and key_resp_8.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_8.tStart = t  # not accounting for scr refresh
        key_resp_8.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_8, 'tStartRefresh')  # time at next scr refresh
        key_resp_8.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_8.clock.reset)  # t=0 on next screen flip
        key_resp_8.clearEvents(eventType='keyboard')
    if key_resp_8.status == STARTED:
        theseKeys = key_resp_8.getKeys(keyList=['1'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_8.keys = theseKeys.name  # just the last key pressed
            key_resp_8.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in ExFeedback1Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "ExFeedback1"-------
for thisComponent in ExFeedback1Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_8.keys in ['', [], None]:  # No response was made
    key_resp_8.keys = None
thisExp.addData('key_resp_8.keys',key_resp_8.keys)
if key_resp_8.keys != None:  # we had a response
    thisExp.addData('key_resp_8.rt', key_resp_8.rt)
thisExp.nextEntry()
# the Routine "ExFeedback1" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "ExDisplay2"-------
t = 0
ExDisplay2Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_9 = keyboard.Keyboard()
# keep track of which components have finished
ExDisplay2Components = [exdisplay2, key_resp_9]
for thisComponent in ExDisplay2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "ExDisplay2"-------
while continueRoutine:
    # get current time
    t = ExDisplay2Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *exdisplay2* updates
    if t >= 0.0 and exdisplay2.status == NOT_STARTED:
        # keep track of start time/frame for later
        exdisplay2.tStart = t  # not accounting for scr refresh
        exdisplay2.frameNStart = frameN  # exact frame index
        win.timeOnFlip(exdisplay2, 'tStartRefresh')  # time at next scr refresh
        exdisplay2.setAutoDraw(True)
    
    # *key_resp_9* updates
    if t >= 0.0 and key_resp_9.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_9.tStart = t  # not accounting for scr refresh
        key_resp_9.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_9, 'tStartRefresh')  # time at next scr refresh
        key_resp_9.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_9.clock.reset)  # t=0 on next screen flip
        key_resp_9.clearEvents(eventType='keyboard')
    if key_resp_9.status == STARTED:
        theseKeys = key_resp_9.getKeys(keyList=['2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_9.keys = theseKeys.name  # just the last key pressed
            key_resp_9.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in ExDisplay2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "ExDisplay2"-------
for thisComponent in ExDisplay2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_9.keys in ['', [], None]:  # No response was made
    key_resp_9.keys = None
thisExp.addData('key_resp_9.keys',key_resp_9.keys)
if key_resp_9.keys != None:  # we had a response
    thisExp.addData('key_resp_9.rt', key_resp_9.rt)
thisExp.nextEntry()
# the Routine "ExDisplay2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "ExChoice2"-------
t = 0
ExChoice2Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_10 = keyboard.Keyboard()
# keep track of which components have finished
ExChoice2Components = [exchoice2, key_resp_10]
for thisComponent in ExChoice2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "ExChoice2"-------
while continueRoutine:
    # get current time
    t = ExChoice2Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *exchoice2* updates
    if t >= 0.0 and exchoice2.status == NOT_STARTED:
        # keep track of start time/frame for later
        exchoice2.tStart = t  # not accounting for scr refresh
        exchoice2.frameNStart = frameN  # exact frame index
        win.timeOnFlip(exchoice2, 'tStartRefresh')  # time at next scr refresh
        exchoice2.setAutoDraw(True)
    
    # *key_resp_10* updates
    if t >= 0.0 and key_resp_10.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_10.tStart = t  # not accounting for scr refresh
        key_resp_10.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_10, 'tStartRefresh')  # time at next scr refresh
        key_resp_10.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_10.clock.reset)  # t=0 on next screen flip
        key_resp_10.clearEvents(eventType='keyboard')
    if key_resp_10.status == STARTED:
        theseKeys = key_resp_10.getKeys(keyList=['1'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_10.keys = theseKeys.name  # just the last key pressed
            key_resp_10.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in ExChoice2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "ExChoice2"-------
for thisComponent in ExChoice2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_10.keys in ['', [], None]:  # No response was made
    key_resp_10.keys = None
thisExp.addData('key_resp_10.keys',key_resp_10.keys)
if key_resp_10.keys != None:  # we had a response
    thisExp.addData('key_resp_10.rt', key_resp_10.rt)
thisExp.nextEntry()
# the Routine "ExChoice2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "ExFeedback2"-------
t = 0
ExFeedback2Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_11 = keyboard.Keyboard()
# keep track of which components have finished
ExFeedback2Components = [exfeedback2, key_resp_11]
for thisComponent in ExFeedback2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "ExFeedback2"-------
while continueRoutine:
    # get current time
    t = ExFeedback2Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *exfeedback2* updates
    if t >= 0.0 and exfeedback2.status == NOT_STARTED:
        # keep track of start time/frame for later
        exfeedback2.tStart = t  # not accounting for scr refresh
        exfeedback2.frameNStart = frameN  # exact frame index
        win.timeOnFlip(exfeedback2, 'tStartRefresh')  # time at next scr refresh
        exfeedback2.setAutoDraw(True)
    
    # *key_resp_11* updates
    if t >= 0.0 and key_resp_11.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_11.tStart = t  # not accounting for scr refresh
        key_resp_11.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_11, 'tStartRefresh')  # time at next scr refresh
        key_resp_11.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_11.clock.reset)  # t=0 on next screen flip
        key_resp_11.clearEvents(eventType='keyboard')
    if key_resp_11.status == STARTED:
        theseKeys = key_resp_11.getKeys(keyList=['2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_11.keys = theseKeys.name  # just the last key pressed
            key_resp_11.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in ExFeedback2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "ExFeedback2"-------
for thisComponent in ExFeedback2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_11.keys in ['', [], None]:  # No response was made
    key_resp_11.keys = None
thisExp.addData('key_resp_11.keys',key_resp_11.keys)
if key_resp_11.keys != None:  # we had a response
    thisExp.addData('key_resp_11.rt', key_resp_11.rt)
thisExp.nextEntry()
# the Routine "ExFeedback2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instruction6"-------
t = 0
Instruction6Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_12 = keyboard.Keyboard()
# keep track of which components have finished
Instruction6Components = [instruction6, key_resp_12]
for thisComponent in Instruction6Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Instruction6"-------
while continueRoutine:
    # get current time
    t = Instruction6Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instruction6* updates
    if t >= 0.0 and instruction6.status == NOT_STARTED:
        # keep track of start time/frame for later
        instruction6.tStart = t  # not accounting for scr refresh
        instruction6.frameNStart = frameN  # exact frame index
        win.timeOnFlip(instruction6, 'tStartRefresh')  # time at next scr refresh
        instruction6.setAutoDraw(True)
    
    # *key_resp_12* updates
    if t >= 0.0 and key_resp_12.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_12.tStart = t  # not accounting for scr refresh
        key_resp_12.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_12, 'tStartRefresh')  # time at next scr refresh
        key_resp_12.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_12.clock.reset)  # t=0 on next screen flip
        key_resp_12.clearEvents(eventType='keyboard')
    if key_resp_12.status == STARTED:
        theseKeys = key_resp_12.getKeys(keyList=['1'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_12.keys = theseKeys.name  # just the last key pressed
            key_resp_12.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instruction6Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instruction6"-------
for thisComponent in Instruction6Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_12.keys in ['', [], None]:  # No response was made
    key_resp_12.keys = None
thisExp.addData('key_resp_12.keys',key_resp_12.keys)
if key_resp_12.keys != None:  # we had a response
    thisExp.addData('key_resp_12.rt', key_resp_12.rt)
thisExp.nextEntry()
# the Routine "Instruction6" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instruction7"-------
t = 0
Instruction7Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_13 = keyboard.Keyboard()
# keep track of which components have finished
Instruction7Components = [instruction7, key_resp_13]
for thisComponent in Instruction7Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Instruction7"-------
while continueRoutine:
    # get current time
    t = Instruction7Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *instruction7* updates
    if t >= 0.0 and instruction7.status == NOT_STARTED:
        # keep track of start time/frame for later
        instruction7.tStart = t  # not accounting for scr refresh
        instruction7.frameNStart = frameN  # exact frame index
        win.timeOnFlip(instruction7, 'tStartRefresh')  # time at next scr refresh
        instruction7.setAutoDraw(True)
    
    # *key_resp_13* updates
    if t >= 0.0 and key_resp_13.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_13.tStart = t  # not accounting for scr refresh
        key_resp_13.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_13, 'tStartRefresh')  # time at next scr refresh
        key_resp_13.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_13.clock.reset)  # t=0 on next screen flip
        key_resp_13.clearEvents(eventType='keyboard')
    if key_resp_13.status == STARTED:
        theseKeys = key_resp_13.getKeys(keyList=['1'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_13.keys = theseKeys.name  # just the last key pressed
            key_resp_13.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instruction7Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instruction7"-------
for thisComponent in Instruction7Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('instruction7.started', instruction7.tStartRefresh)
thisExp.addData('instruction7.stopped', instruction7.tStopRefresh)
# check responses
if key_resp_13.keys in ['', [], None]:  # No response was made
    key_resp_13.keys = None
thisExp.addData('key_resp_13.keys',key_resp_13.keys)
if key_resp_13.keys != None:  # we had a response
    thisExp.addData('key_resp_13.rt', key_resp_13.rt)
thisExp.nextEntry()
# the Routine "Instruction7" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "LetUsGo"-------
t = 0
LetUsGoClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_4 = keyboard.Keyboard()
# keep track of which components have finished
LetUsGoComponents = [letusgo, key_resp_4]
for thisComponent in LetUsGoComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "LetUsGo"-------
while continueRoutine:
    # get current time
    t = LetUsGoClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *letusgo* updates
    if t >= 0.0 and letusgo.status == NOT_STARTED:
        # keep track of start time/frame for later
        letusgo.tStart = t  # not accounting for scr refresh
        letusgo.frameNStart = frameN  # exact frame index
        win.timeOnFlip(letusgo, 'tStartRefresh')  # time at next scr refresh
        letusgo.setAutoDraw(True)
    
    # *key_resp_4* updates
    if t >= 0.0 and key_resp_4.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_4.tStart = t  # not accounting for scr refresh
        key_resp_4.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_4, 'tStartRefresh')  # time at next scr refresh
        key_resp_4.status = STARTED
        # keyboard checking is just starting
        key_resp_4.clearEvents(eventType='keyboard')
    if key_resp_4.status == STARTED:
        theseKeys = key_resp_4.getKeys(keyList=['1', '2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in LetUsGoComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "LetUsGo"-------
for thisComponent in LetUsGoComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# the Routine "LetUsGo" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
trials = data.TrialHandler(nReps=1, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('BML_practice1.xlsx'),
    seed=None, name='trials')
thisExp.addLoop(trials)  # add the loop to the experiment
thisTrial = trials.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
if thisTrial != None:
    for paramName in thisTrial:
        exec('{} = thisTrial[paramName]'.format(paramName))

for thisTrial in trials:
    currentLoop = trials
    # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
    if thisTrial != None:
        for paramName in thisTrial:
            exec('{} = thisTrial[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "Choice"-------
    t = 0
    ChoiceClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    # update component parameters for each repeat
    Choice_txt = '          {} points\n\n{}% win      {}% lose\n\n  Accept         Decline  '.format(Value,Win,Lose)
    choice.setText(Choice_txt)
    choice_key = keyboard.Keyboard()
    # keep track of which components have finished
    ChoiceComponents = [choice, choice_key]
    for thisComponent in ChoiceComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "Choice"-------
    while continueRoutine:
        # get current time
        t = ChoiceClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *choice* updates
        if t >= 0.0 and choice.status == NOT_STARTED:
            # keep track of start time/frame for later
            choice.tStart = t  # not accounting for scr refresh
            choice.frameNStart = frameN  # exact frame index
            win.timeOnFlip(choice, 'tStartRefresh')  # time at next scr refresh
            choice.setAutoDraw(True)
        
        # *choice_key* updates
        if t >= 0.0 and choice_key.status == NOT_STARTED:
            # keep track of start time/frame for later
            choice_key.tStart = t  # not accounting for scr refresh
            choice_key.frameNStart = frameN  # exact frame index
            win.timeOnFlip(choice_key, 'tStartRefresh')  # time at next scr refresh
            choice_key.status = STARTED
            # keyboard checking is just starting
            win.callOnFlip(choice_key.clock.reset)  # t=0 on next screen flip
            choice_key.clearEvents(eventType='keyboard')
        if choice_key.status == STARTED:
            theseKeys = choice_key.getKeys(keyList=['1', '2'], waitRelease=False)
            if len(theseKeys):
                theseKeys = theseKeys[0]  # at least one key was pressed
                
                # check for quit:
                if "escape" == theseKeys:
                    endExpNow = True
                if choice_key.keys == []:  # then this was the first keypress
                    choice_key.keys = theseKeys.name  # just the first key pressed
                    choice_key.rt = theseKeys.rt
                    # a response ends the routine
                    continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in ChoiceComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Choice"-------
    for thisComponent in ChoiceComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # check responses
    if choice_key.keys in ['', [], None]:  # No response was made
        choice_key.keys = None
    trials.addData('choice_key.keys',choice_key.keys)
    if choice_key.keys != None:  # we had a response
        trials.addData('choice_key.rt', choice_key.rt)
    trials.addData('choice_key.started', choice_key.tStartRefresh)
    trials.addData('choice_key.stopped', choice_key.tStopRefresh)
    theseKeys = choice_key.keys
    
    if theseKeys == '1':
        LBracket = ""
        RBracket = ""
        if RealValue > 0:
            Gainsign = '+'
        else:
            Gainsign = ''
        Outcome = RealValue
        RespCheck = ""
        totalsum = totalsum + Outcome
    elif theseKeys == '2':
        LBracket = "("
        RBracket = ")"
        if RealValue > 0:
            Gainsign = '+'
        else:
            Gainsign = ''
        Outcome = RealValue
        RespCheck = ""
        totalsum = totalsum
    elif theseKeys == None:
        LBracket = ""
        RBracket = ""
        Gainsign = ""
        Outcome = 0
        RespCheck = "Please respond!!!"
        totalsum = totalsum
    
    thisExp.addData('LBracket', LBracket)
    thisExp.addData('RBracket', RBracket)
    thisExp.addData('Gainsign', Gainsign)
    thisExp.addData('RespCheck', RespCheck)
    thisExp.addData('Outcome', Outcome)
    thisExp.addData('totalsum', totalsum)
    
    # the Routine "Choice" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # ------Prepare to start Routine "ITI1"-------
    t = 0
    ITI1Clock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(1.000000)
    # update component parameters for each repeat
    # keep track of which components have finished
    ITI1Components = [iti1]
    for thisComponent in ITI1Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "ITI1"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = ITI1Clock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *iti1* updates
        if t >= 0.0 and iti1.status == NOT_STARTED:
            # keep track of start time/frame for later
            iti1.tStart = t  # not accounting for scr refresh
            iti1.frameNStart = frameN  # exact frame index
            win.timeOnFlip(iti1, 'tStartRefresh')  # time at next scr refresh
            iti1.setAutoDraw(True)
        frameRemains = 0.0 + 1- win.monitorFramePeriod * 0.75  # most of one frame period left
        if iti1.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            iti1.tStop = t  # not accounting for scr refresh
            iti1.frameNStop = frameN  # exact frame index
            win.timeOnFlip(iti1, 'tStopRefresh')  # time at next scr refresh
            iti1.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in ITI1Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "ITI1"-------
    for thisComponent in ITI1Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    
    # ------Prepare to start Routine "Feedback"-------
    t = 0
    FeedbackClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(2.000000)
    # update component parameters for each repeat
    feedback_txt = '     {}{}{}{} points\n {}\n\n total {} points'.format(LBracket,Gainsign,Outcome,RBracket,RespCheck,totalsum)
    feedback.setText(feedback_txt)
    # keep track of which components have finished
    FeedbackComponents = [feedback]
    for thisComponent in FeedbackComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "Feedback"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = FeedbackClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *feedback* updates
        if t >= 0.0 and feedback.status == NOT_STARTED:
            # keep track of start time/frame for later
            feedback.tStart = t  # not accounting for scr refresh
            feedback.frameNStart = frameN  # exact frame index
            win.timeOnFlip(feedback, 'tStartRefresh')  # time at next scr refresh
            feedback.setAutoDraw(True)
        frameRemains = 0.0 + 2.0- win.monitorFramePeriod * 0.75  # most of one frame period left
        if feedback.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            feedback.tStop = t  # not accounting for scr refresh
            feedback.frameNStop = frameN  # exact frame index
            win.timeOnFlip(feedback, 'tStopRefresh')  # time at next scr refresh
            feedback.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in FeedbackComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Feedback"-------
    for thisComponent in FeedbackComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    trials.addData('feedback.started', feedback.tStartRefresh)
    trials.addData('feedback.stopped', feedback.tStopRefresh)
    if choice_key.keys == '1':
        if RealValue > 0:
            cond = 'A_GAIN'
        elif RealValue < 0:
            cond = 'A_LOSS'
    elif choice_key.keys == '2':
        if RealValue > 0:
            cond = 'R_GAIN'
        elif RealValue < 0:
            cond = 'R_LOSS'
    elif choice_key.keys == None:
        cond = 'Dummy'
    
    thisExp.addData('cond', cond)
    
    
    # ------Prepare to start Routine "ITI2"-------
    t = 0
    ITI2Clock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(1.000000)
    # update component parameters for each repeat
    # keep track of which components have finished
    ITI2Components = [iti2]
    for thisComponent in ITI2Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "ITI2"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = ITI2Clock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *iti2* updates
        if t >= 0.0 and iti2.status == NOT_STARTED:
            # keep track of start time/frame for later
            iti2.tStart = t  # not accounting for scr refresh
            iti2.frameNStart = frameN  # exact frame index
            win.timeOnFlip(iti2, 'tStartRefresh')  # time at next scr refresh
            iti2.setAutoDraw(True)
        frameRemains = 0.0 + 1- win.monitorFramePeriod * 0.75  # most of one frame period left
        if iti2.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            iti2.tStop = t  # not accounting for scr refresh
            iti2.frameNStop = frameN  # exact frame index
            win.timeOnFlip(iti2, 'tStopRefresh')  # time at next scr refresh
            iti2.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in ITI2Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "ITI2"-------
    for thisComponent in ITI2Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.nextEntry()
    
# completed 1 repeats of 'trials'


# ------Prepare to start Routine "LetUsGo2"-------
t = 0
LetUsGo2Clock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_14 = keyboard.Keyboard()
# keep track of which components have finished
LetUsGo2Components = [letusgo2, key_resp_14]
for thisComponent in LetUsGo2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "LetUsGo2"-------
while continueRoutine:
    # get current time
    t = LetUsGo2Clock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *letusgo2* updates
    if t >= 0.0 and letusgo2.status == NOT_STARTED:
        # keep track of start time/frame for later
        letusgo2.tStart = t  # not accounting for scr refresh
        letusgo2.frameNStart = frameN  # exact frame index
        win.timeOnFlip(letusgo2, 'tStartRefresh')  # time at next scr refresh
        letusgo2.setAutoDraw(True)
    
    # *key_resp_14* updates
    if t >= 0.0 and key_resp_14.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_14.tStart = t  # not accounting for scr refresh
        key_resp_14.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_14, 'tStartRefresh')  # time at next scr refresh
        key_resp_14.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_14.clock.reset)  # t=0 on next screen flip
        key_resp_14.clearEvents(eventType='keyboard')
    if key_resp_14.status == STARTED:
        theseKeys = key_resp_14.getKeys(keyList=['1', '2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_14.keys = theseKeys.name  # just the last key pressed
            key_resp_14.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in LetUsGo2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "LetUsGo2"-------
for thisComponent in LetUsGo2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_14.keys in ['', [], None]:  # No response was made
    key_resp_14.keys = None
thisExp.addData('key_resp_14.keys',key_resp_14.keys)
if key_resp_14.keys != None:  # we had a response
    thisExp.addData('key_resp_14.rt', key_resp_14.rt)
thisExp.nextEntry()
# the Routine "LetUsGo2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
trials_2 = data.TrialHandler(nReps=1, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('BML_practice2.xlsx'),
    seed=None, name='trials_2')
thisExp.addLoop(trials_2)  # add the loop to the experiment
thisTrial_2 = trials_2.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisTrial_2.rgb)
if thisTrial_2 != None:
    for paramName in thisTrial_2:
        exec('{} = thisTrial_2[paramName]'.format(paramName))

for thisTrial_2 in trials_2:
    currentLoop = trials_2
    # abbreviate parameter names if possible (e.g. rgb = thisTrial_2.rgb)
    if thisTrial_2 != None:
        for paramName in thisTrial_2:
            exec('{} = thisTrial_2[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "Choice2"-------
    t = 0
    Choice2Clock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(4.000000)
    # update component parameters for each repeat
    Choice2_txt = '          {} points\n\n{}% win      {}% lose\n\n  Accept         Decline  '.format(Value,Win,Lose)
    choice2.setText(Choice2_txt)
    choice_2_key = keyboard.Keyboard()
    # keep track of which components have finished
    Choice2Components = [choice2, choice_2_key]
    for thisComponent in Choice2Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "Choice2"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = Choice2Clock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *choice2* updates
        if t >= 0.0 and choice2.status == NOT_STARTED:
            # keep track of start time/frame for later
            choice2.tStart = t  # not accounting for scr refresh
            choice2.frameNStart = frameN  # exact frame index
            win.timeOnFlip(choice2, 'tStartRefresh')  # time at next scr refresh
            choice2.setAutoDraw(True)
        frameRemains = 0.0 + 4.0- win.monitorFramePeriod * 0.75  # most of one frame period left
        if choice2.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            choice2.tStop = t  # not accounting for scr refresh
            choice2.frameNStop = frameN  # exact frame index
            win.timeOnFlip(choice2, 'tStopRefresh')  # time at next scr refresh
            choice2.setAutoDraw(False)
        
        # *choice_2_key* updates
        if t >= 0.0 and choice_2_key.status == NOT_STARTED:
            # keep track of start time/frame for later
            choice_2_key.tStart = t  # not accounting for scr refresh
            choice_2_key.frameNStart = frameN  # exact frame index
            win.timeOnFlip(choice_2_key, 'tStartRefresh')  # time at next scr refresh
            choice_2_key.status = STARTED
            # keyboard checking is just starting
            win.callOnFlip(choice_2_key.clock.reset)  # t=0 on next screen flip
            choice_2_key.clearEvents(eventType='keyboard')
        frameRemains = 0.0 + 4- win.monitorFramePeriod * 0.75  # most of one frame period left
        if choice_2_key.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            choice_2_key.tStop = t  # not accounting for scr refresh
            choice_2_key.frameNStop = frameN  # exact frame index
            win.timeOnFlip(choice_2_key, 'tStopRefresh')  # time at next scr refresh
            choice_2_key.status = FINISHED
        if choice_2_key.status == STARTED:
            theseKeys = choice_2_key.getKeys(keyList=['1', '2'], waitRelease=False)
            if len(theseKeys):
                theseKeys = theseKeys[0]  # at least one key was pressed
                
                # check for quit:
                if "escape" == theseKeys:
                    endExpNow = True
                if choice_2_key.keys == []:  # then this was the first keypress
                    choice_2_key.keys = theseKeys.name  # just the first key pressed
                    choice_2_key.rt = theseKeys.rt
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Choice2Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Choice2"-------
    for thisComponent in Choice2Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # check responses
    if choice_2_key.keys in ['', [], None]:  # No response was made
        choice_2_key.keys = None
    trials_2.addData('choice_2_key.keys',choice_2_key.keys)
    if choice_2_key.keys != None:  # we had a response
        trials_2.addData('choice_2_key.rt', choice_2_key.rt)
    trials_2.addData('choice_2_key.started', choice_2_key.tStartRefresh)
    trials_2.addData('choice_2_key.stopped', choice_2_key.tStopRefresh)
    theseKeys = choice_2_key.keys
    
    if theseKeys == '1':
        LBracket = ""
        RBracket = ""
        if RealValue > 0:
            Gainsign = '+'
        else:
            Gainsign = ''
        Outcome = RealValue
        RespCheck = ""
        totalsum = totalsum + Outcome
    elif theseKeys == '2':
        LBracket = "("
        RBracket = ")"
        if RealValue > 0:
            Gainsign = '+'
        else:
            Gainsign = ''
        Outcome = RealValue
        RespCheck = ""
        totalsum = totalsum
    elif theseKeys == None:
        LBracket = ""
        RBracket = ""
        Gainsign = ""
        Outcome = 0
        RespCheck = "Please respond!!!"
        totalsum = totalsum
    
    thisExp.addData('LBracket', LBracket)
    thisExp.addData('RBracket', RBracket)
    thisExp.addData('Gainsign', Gainsign)
    thisExp.addData('RespCheck', RespCheck)
    thisExp.addData('Outcome', Outcome)
    thisExp.addData('totalsum', totalsum)
    
    
    # ------Prepare to start Routine "ITI1"-------
    t = 0
    ITI1Clock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(1.000000)
    # update component parameters for each repeat
    # keep track of which components have finished
    ITI1Components = [iti1]
    for thisComponent in ITI1Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "ITI1"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = ITI1Clock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *iti1* updates
        if t >= 0.0 and iti1.status == NOT_STARTED:
            # keep track of start time/frame for later
            iti1.tStart = t  # not accounting for scr refresh
            iti1.frameNStart = frameN  # exact frame index
            win.timeOnFlip(iti1, 'tStartRefresh')  # time at next scr refresh
            iti1.setAutoDraw(True)
        frameRemains = 0.0 + 1- win.monitorFramePeriod * 0.75  # most of one frame period left
        if iti1.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            iti1.tStop = t  # not accounting for scr refresh
            iti1.frameNStop = frameN  # exact frame index
            win.timeOnFlip(iti1, 'tStopRefresh')  # time at next scr refresh
            iti1.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in ITI1Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "ITI1"-------
    for thisComponent in ITI1Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    
    # ------Prepare to start Routine "Feedback"-------
    t = 0
    FeedbackClock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(2.000000)
    # update component parameters for each repeat
    feedback_txt = '     {}{}{}{} points\n {}\n\n total {} points'.format(LBracket,Gainsign,Outcome,RBracket,RespCheck,totalsum)
    feedback.setText(feedback_txt)
    # keep track of which components have finished
    FeedbackComponents = [feedback]
    for thisComponent in FeedbackComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "Feedback"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = FeedbackClock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *feedback* updates
        if t >= 0.0 and feedback.status == NOT_STARTED:
            # keep track of start time/frame for later
            feedback.tStart = t  # not accounting for scr refresh
            feedback.frameNStart = frameN  # exact frame index
            win.timeOnFlip(feedback, 'tStartRefresh')  # time at next scr refresh
            feedback.setAutoDraw(True)
        frameRemains = 0.0 + 2.0- win.monitorFramePeriod * 0.75  # most of one frame period left
        if feedback.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            feedback.tStop = t  # not accounting for scr refresh
            feedback.frameNStop = frameN  # exact frame index
            win.timeOnFlip(feedback, 'tStopRefresh')  # time at next scr refresh
            feedback.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in FeedbackComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Feedback"-------
    for thisComponent in FeedbackComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    trials_2.addData('feedback.started', feedback.tStartRefresh)
    trials_2.addData('feedback.stopped', feedback.tStopRefresh)
    if choice_key.keys == '1':
        if RealValue > 0:
            cond = 'A_GAIN'
        elif RealValue < 0:
            cond = 'A_LOSS'
    elif choice_key.keys == '2':
        if RealValue > 0:
            cond = 'R_GAIN'
        elif RealValue < 0:
            cond = 'R_LOSS'
    elif choice_key.keys == None:
        cond = 'Dummy'
    
    thisExp.addData('cond', cond)
    
    
    # ------Prepare to start Routine "ITI2"-------
    t = 0
    ITI2Clock.reset()  # clock
    frameN = -1
    continueRoutine = True
    routineTimer.add(1.000000)
    # update component parameters for each repeat
    # keep track of which components have finished
    ITI2Components = [iti2]
    for thisComponent in ITI2Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    
    # -------Start Routine "ITI2"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = ITI2Clock.getTime()
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *iti2* updates
        if t >= 0.0 and iti2.status == NOT_STARTED:
            # keep track of start time/frame for later
            iti2.tStart = t  # not accounting for scr refresh
            iti2.frameNStart = frameN  # exact frame index
            win.timeOnFlip(iti2, 'tStartRefresh')  # time at next scr refresh
            iti2.setAutoDraw(True)
        frameRemains = 0.0 + 1- win.monitorFramePeriod * 0.75  # most of one frame period left
        if iti2.status == STARTED and t >= frameRemains:
            # keep track of stop time/frame for later
            iti2.tStop = t  # not accounting for scr refresh
            iti2.frameNStop = frameN  # exact frame index
            win.timeOnFlip(iti2, 'tStopRefresh')  # time at next scr refresh
            iti2.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in ITI2Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "ITI2"-------
    for thisComponent in ITI2Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.nextEntry()
    
# completed 1 repeats of 'trials_2'


# ------Prepare to start Routine "WhereToNow"-------
t = 0
WhereToNowClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
wheretonow.setText('Do you want to repeat the practice?\n\n    (Please alert the experimenter)')
key_resp_15 = keyboard.Keyboard()
# keep track of which components have finished
WhereToNowComponents = [wheretonow, key_resp_15]
for thisComponent in WhereToNowComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "WhereToNow"-------
while continueRoutine:
    # get current time
    t = WhereToNowClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *wheretonow* updates
    if t >= 0.0 and wheretonow.status == NOT_STARTED:
        # keep track of start time/frame for later
        wheretonow.tStart = t  # not accounting for scr refresh
        wheretonow.frameNStart = frameN  # exact frame index
        win.timeOnFlip(wheretonow, 'tStartRefresh')  # time at next scr refresh
        wheretonow.setAutoDraw(True)
    
    # *key_resp_15* updates
    if t >= 0.0 and key_resp_15.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_15.tStart = t  # not accounting for scr refresh
        key_resp_15.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_15, 'tStartRefresh')  # time at next scr refresh
        key_resp_15.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_15.clock.reset)  # t=0 on next screen flip
        key_resp_15.clearEvents(eventType='keyboard')
    if key_resp_15.status == STARTED:
        theseKeys = key_resp_15.getKeys(keyList=['1', '2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_15.keys = theseKeys.name  # just the last key pressed
            key_resp_15.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in WhereToNowComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "WhereToNow"-------
for thisComponent in WhereToNowComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_15.keys in ['', [], None]:  # No response was made
    key_resp_15.keys = None
thisExp.addData('key_resp_15.keys',key_resp_15.keys)
if key_resp_15.keys != None:  # we had a response
    thisExp.addData('key_resp_15.rt', key_resp_15.rt)
thisExp.nextEntry()
# the Routine "WhereToNow" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Complete"-------
t = 0
CompleteClock.reset()  # clock
frameN = -1
continueRoutine = True
# update component parameters for each repeat
key_resp_16 = keyboard.Keyboard()
# keep track of which components have finished
CompleteComponents = [complete, key_resp_16]
for thisComponent in CompleteComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED

# -------Start Routine "Complete"-------
while continueRoutine:
    # get current time
    t = CompleteClock.getTime()
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *complete* updates
    if t >= 0.0 and complete.status == NOT_STARTED:
        # keep track of start time/frame for later
        complete.tStart = t  # not accounting for scr refresh
        complete.frameNStart = frameN  # exact frame index
        win.timeOnFlip(complete, 'tStartRefresh')  # time at next scr refresh
        complete.setAutoDraw(True)
    
    # *key_resp_16* updates
    if t >= 0.0 and key_resp_16.status == NOT_STARTED:
        # keep track of start time/frame for later
        key_resp_16.tStart = t  # not accounting for scr refresh
        key_resp_16.frameNStart = frameN  # exact frame index
        win.timeOnFlip(key_resp_16, 'tStartRefresh')  # time at next scr refresh
        key_resp_16.status = STARTED
        # keyboard checking is just starting
        win.callOnFlip(key_resp_16.clock.reset)  # t=0 on next screen flip
        key_resp_16.clearEvents(eventType='keyboard')
    if key_resp_16.status == STARTED:
        theseKeys = key_resp_16.getKeys(keyList=['1', '2'], waitRelease=False)
        if len(theseKeys):
            theseKeys = theseKeys[0]  # at least one key was pressed
            
            # check for quit:
            if "escape" == theseKeys:
                endExpNow = True
            key_resp_16.keys = theseKeys.name  # just the last key pressed
            key_resp_16.rt = theseKeys.rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in CompleteComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Complete"-------
for thisComponent in CompleteComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
# check responses
if key_resp_16.keys in ['', [], None]:  # No response was made
    key_resp_16.keys = None
thisExp.addData('key_resp_16.keys',key_resp_16.keys)
if key_resp_16.keys != None:  # we had a response
    thisExp.addData('key_resp_16.rt', key_resp_16.rt)
thisExp.nextEntry()
# the Routine "Complete" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
