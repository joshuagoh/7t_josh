# Lottery Choice Task (LCT)
These are the different versions of the LCT Psychopy stimuli presentation files.

- **event-related-original**: One of the modified versions of the LCT task used in BML. This is a revision of the original used in BML (3 x 5 mag x prob levels), which itself was revised from the version used in LBN for BLSA (with 2 x 3 prob x mag levels; list A and B). This present revision includes (a) translation to Chinese/English versions (English included here), and (b) different 3 x 5 mag x prob level numbers, and predetermined outcomes.

- **block**: A block version of the event-related design.

- **block-oddball**: Modified from **block** to include an oddball manipulation (extreme magnitude) to induce salience operations. If event-related VAPER@7T successful, it should take precedence that this should be modified to event-related oddball.

- **event-related-lct-sal**: Modified from **event-related-original** to have different stimuli numbers, expanded number of trials, salience trials, and stimuli generator with random kernel seed application (i.e., non-manual generation of true probabilistic and magnitude ranges). This is done for the purpose of testing event-related protocols for VAPER@7T plus examining midbrain system response.

- **event-related-lct-sal-v2**: Modified from **event-related-lct-sal** to have incremental stake magnitudes based on previous run expected values and

## References
1. [Goh, J. O. S., Su, Y.-S., Tang, Y.-J., McCarrey, A. C., Tereshchenko, A., Elkins, W., & Resnick, S. M. (2016). Frontal, Striatal, and Medial Temporal Sensitivity to Value Distinguishes Risk-Taking from Risk-Aversive Older Adults during Decision Making. The Journal of Neuroscience, 36(49), 12498–12509. https://doi.org/10.1523/JNEUROSCI.1386-16.2016](https://www.jneurosci.org/content/36/49/12498)

2. [Su, Y.-S., Chen, J.-T., Tang, Y.-J., Yuan, S.-Y., McCarrey, A. C., & Goh, J. O. S. (2018). Age-related differences in striatal, medial temporal, and frontal involvement during value-based decision processing. Neurobiology of Aging, 69, 185–198. https://doi.org/10.1016/j.neurobiolaging.2018.05.019](https://www.sciencedirect.com/science/article/abs/pii/S0197458018301805?via%3Dihub)

3. [Chuang, Y.-S., Su, Y.-S., & Goh, J. O. S. (2020). Neural responses reveal associations between personal values and value-based decisions. Social Cognitive and Affective Neuroscience, 15(12), 1299–1309. https://doi.org/10.1093/scan/nsaa150](https://academic.oup.com/scan/article/15/11/1217/5956559?login=true)

4. [Chen, H.-Y., Dix, A., Goh, J. O. S., Smolka, M. N., Thurm, F., & Li, S.-C. (2021). Effects and mechanisms of information saliency in enhancing value-based decision-making in younger and older adults. Neurobiology of Aging, 99, 86–98. https://doi.org/10.1016/j.neurobiolaging.2020.11.018](https://www.sciencedirect.com/science/article/abs/pii/S0197458020304085?via%3Dihub)

5. [Lee, C.-Y., Chen, C.-C., Mair, R. W., Gutchess, A., & Goh, J. O. S. (2021). Culture-related differences in the neural processing of probability during mixed lottery value-based decision-making. Biological Psychology, 166, 108209. https://doi.org/10.1016/j.biopsycho.2021.108209](https://www.sciencedirect.com/science/article/abs/pii/S0301051121002027?via%3Dihub)

6. [Chen, P., Hung, H.-Y., & Goh, J. O. S. (2023). Age-related differences in ERP correlates of value-based decision making. Neurobiology of Aging, 123, 10–22. https://doi.org/10.1016/j.neurobiolaging.2022.11.008](https://www.sciencedirect.com/science/article/abs/pii/S0197458022002378?via%3Dihub)

7. [Lee, C.-Y., Goh, J. O. S., & Gau, S. S.-F. (2023). Differential neural processing of value during decision-making in adults with attention-deficit/hyperactivity disorder and healthy controls. Journal of Psychiatry & Neuroscience, 48(2), E115–E124. https://doi.org/10.1503/jpn.220123](https://www.jpn.ca/content/48/2/E115)
