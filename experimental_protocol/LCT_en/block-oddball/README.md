# Lottery Choice Task (LCT) block oddball design
These are the instructions on how to run the LCT block design files contained in this directory. These stimuli presentation files require [Psychopy](https://www.psychopy.org/) to be installed on your local system.

This LCT block oddball task requires that participants have completed the basic LCT block design. There is thus no practice needed as the goal is the same. The key difference is the introduction of super large magnitudes (super salience) in oddball trials. The goal is to induce locus coeruleus type (epinephrinergic) processing.

There are four experimental runs and one practice run in this protocol associated with the following files:

- ADMT_r1.psyexp
- ADMT_r2.psyexp
- ADMT_r3.psyexp
- ADMT_r4.psyexp

These .psyexp files call table values from the following spreadsheets:

- BML_r1.xlsx
- BML_r2.xlsx
- BML_r3.xlsx
- BML_r4.xlsx

The other files are scripts and log files generated when the stimuli presentation code in the psyexp files is run.

For **stimuli timing and sequence details**, please consult [LCT general README.md](experimental_protocol/LCT_en/README.md).

## To run
1. Open Psychopy.
2. Open the psyexp file you wish to run.
3. Click the play button (right pointing triangle).
4. The session info screen will appear for which you must enter the following:
  - **Participant ID code**. 9999 is an example for pilot participants.
  - **Session ID**. Leave default as 001 for first instance of the run. If run is repeated, you can enter 002 as necessary to avoid overwrite.
  - **Start points**. This is the total accumulated points that the participant has at the beginning of the run. If this is the first time running any run, then the start points are 0. If the participant has completed prior runs, you can note the last accumulated score from the latest run and enter it in this field. If the latest score is forgotten, you can open /data/*_ADMT_*.csv, scroll right and bottom to the 'totalsum' column to get the latest score.
5. Timings and responses will be logged in /data/*_ADMT_*.csv per run.
