# Evaluation of 7T MRI @UIUC
The higher B0 field strength of the 7T system allows for higher spatial resolutions to be achieved more effectively. This project evaluates the data afforded by the improved spatial resolution of the 7T Siemens (Terra) system located at Carle Hospital, Urbana, IL, administered by the Biomedical Imaging Center (BIC)@Beckman Institute, UIUC.

The work done here was initiated during Josh's Sabbatical period between March 1 to May 31 2024.

## [Data](data/)
Data acquired for this project (pointers only on online repo; data on local servers)

## [Experimental Protocol](experimental_protocol/)
Contains the stimuli presentation materials used in this project.

## References
**Population Study using 7T@UIUC**
1. [Champaign-Urbana Population Study](https://cupopulationstudy.illinois.edu/about)

**Studies using the Lottery Choice Task experimental protocol evaluated here**

2. [Goh, J. O. S., Su, Y.-S., Tang, Y.-J., McCarrey, A. C., Tereshchenko, A., Elkins, W., & Resnick, S. M. (2016). Frontal, Striatal, and Medial Temporal Sensitivity to Value Distinguishes Risk-Taking from Risk-Aversive Older Adults during Decision Making. The Journal of Neuroscience, 36(49), 12498–12509. https://doi.org/10.1523/JNEUROSCI.1386-16.2016](https://www.jneurosci.org/content/36/49/12498)

3. [Su, Y.-S., Chen, J.-T., Tang, Y.-J., Yuan, S.-Y., McCarrey, A. C., & Goh, J. O. S. (2018). Age-related differences in striatal, medial temporal, and frontal involvement during value-based decision processing. Neurobiology of Aging, 69, 185–198. https://doi.org/10.1016/j.neurobiolaging.2018.05.019](https://www.sciencedirect.com/science/article/abs/pii/S0197458018301805?via%3Dihub)

4. [Chuang, Y.-S., Su, Y.-S., & Goh, J. O. S. (2020). Neural responses reveal associations between personal values and value-based decisions. Social Cognitive and Affective Neuroscience, 15(12), 1299–1309. https://doi.org/10.1093/scan/nsaa150](https://academic.oup.com/scan/article/15/11/1217/5956559?login=true)

5. [Chen, H.-Y., Dix, A., Goh, J. O. S., Smolka, M. N., Thurm, F., & Li, S.-C. (2021). Effects and mechanisms of information saliency in enhancing value-based decision-making in younger and older adults. Neurobiology of Aging, 99, 86–98. https://doi.org/10.1016/j.neurobiolaging.2020.11.018](https://www.sciencedirect.com/science/article/abs/pii/S0197458020304085?via%3Dihub)

6. [Lee, C.-Y., Chen, C.-C., Mair, R. W., Gutchess, A., & Goh, J. O. S. (2021). Culture-related differences in the neural processing of probability during mixed lottery value-based decision-making. Biological Psychology, 166, 108209. https://doi.org/10.1016/j.biopsycho.2021.108209](https://www.sciencedirect.com/science/article/abs/pii/S0301051121002027?via%3Dihub)

7. [Chen, P., Hung, H.-Y., & Goh, J. O. S. (2023). Age-related differences in ERP correlates of value-based decision making. Neurobiology of Aging, 123, 10–22. https://doi.org/10.1016/j.neurobiolaging.2022.11.008](https://www.sciencedirect.com/science/article/abs/pii/S0197458022002378?via%3Dihub)

8. [Lee, C.-Y., Goh, J. O. S., & Gau, S. S.-F. (2023). Differential neural processing of value during decision-making in adults with attention-deficit/hyperactivity disorder and healthy controls. Journal of Psychiatry & Neuroscience, 48(2), E115–E124. https://doi.org/10.1503/jpn.220123](https://www.jpn.ca/content/48/2/E115)

**VAPER studies**

9. [Chai, Y., Morgan, A. T., Xie, H., Li, L., Huber, L., Bandettini, P. A., & Sutton, B. P. (2024). Unlocking near-whole brain, layer-specific functional connectivity with 3D VAPER fMRI. Imaging Neuroscience. https://doi.org/10.1162/imag_a_00140](https://direct.mit.edu/imag/article/doi/10.1162/imag_a_00140/120467/Unlocking-near-whole-brain-layer-specific)

10. [Chai, Y., Li, L., Wang, Y., Huber, L., Poser, B. A., Duyn, J., & Bandettini, P. A. (2021). Magnetization transfer weighted EPI facilitates cortical depth determination in native fMRI space. Neuroimage, 242, 118455. https://doi.org/10.1016/j.neuroimage.2021.118455](https://www.sciencedirect.com/science/article/pii/S1053811921007291?via%3Dihub)

11. [Li, L., Law, C., Marrett, S., Chai, Y., Huber, L., Jezzard, P., & Bandettini, P. (2022). Quantification of cerebral blood volume changes caused by visual stimulation at 3 T using DANTE-prepared dual-echo EPI. Magnetic Resonance in Medicine, 87(4), 1846–1862. https://doi.org/10.1002/mrm.29099](https://onlinelibrary.wiley.com/doi/10.1002/mrm.29099)

12. [Chai, Y., Li, L., Huber, L., Poser, B. A., & Bandettini, P. A. (2020). Integrated VASO and perfusion contrast: A new tool for laminar functional MRI. Neuroimage, 207, 116358. https://doi.org/10.1016/j.neuroimage.2019.116358](https://www.sciencedirect.com/science/article/pii/S1053811919309498?via%3Dihub)

**Neuromelanin**

13. [Sulzer, D., Cassidy, C., Horga, G., Kang, U. J., Fahn, S., Casella, L., … Zecca, L. (2018). Neuromelanin detection by magnetic resonance imaging (MRI) and its promise as a biomarker for Parkinson’s disease. Npj Parkinson’s Disease, 4, 11. https://doi.org/10.1038/s41531-018-0047-3](https://www.nature.com/articles/s41531-018-0047-3)