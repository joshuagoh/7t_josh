# Documentation on brain image processing
Motion correction
- Interpolation (est, res) -> 5th degree
- Qual, Sep, Smooth -> 1

epi, mt realign in one step.

epic, mt split into odd and even volumes.

mt, average odd - average even / (average even +100)

Denoise ANTs

AFNI GLM


# References
- [Richter, A., Reinhard, F., Kraemer, B., & Gruber, O. (2020). A high-resolution fMRI approach to characterize functionally distinct neural pathways within dopaminergic midbrain and nucleus accumbens during reward and salience processing. European Neuropsychopharmacology, 36, 137–150. https://doi.org/10.1016/j.euroneuro.2020.05.005](docs/Richter et al. 2020.pdf)

- [Chai, Y., Li, L., Huber, L., Poser, B. A., & Bandettini, P. A. (2020). Integrated VASO and perfusion contrast: A new tool for laminar functional MRI. Neuroimage, 207, 116358. https://doi.org/10.1016/j.neuroimage.2019.116358](https://www.sciencedirect.com/science/article/pii/S1053811919309498?via%3Dihub)

- [Li, L., Law, C., Marrett, S., Chai, Y., Huber, L., Jezzard, P., & Bandettini, P. (2022). Quantification of cerebral blood volume changes caused by visual stimulation at 3 T using DANTE-prepared dual-echo EPI. Magnetic Resonance in Medicine, 87(4), 1846–1862. https://doi.org/10.1002/mrm.29099](https://onlinelibrary.wiley.com/doi/10.1002/mrm.29099)

- [Chai, Y., Morgan, A. T., Xie, H., Li, L., Huber, L., Bandettini, P. A., & Sutton, B. P. (2024). Unlocking near-whole brain, layer-specific functional connectivity with 3D VAPER fMRI. Imaging Neuroscience. https://doi.org/10.1162/imag_a_00140](https://direct.mit.edu/imag/article/doi/10.1162/imag_a_00140/120467/Unlocking-near-whole-brain-layer-specific)
